package com.pms.sentek.controller;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pms.common.controller.BaseController;
import com.pms.common.exception.HeaderException;
import com.pms.common.pojo.PMSEmployeeCashClaimsHdPojo;

@RestController
@RequestMapping("/employeeCashClaims")
public class PMSEmployeeCashClaimsController extends BaseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSEmployeeCashClaimsController.class);

	@Autowired
	com.pms.sentek.service.PMSEmployeeCashClaimsService eccService;
	
	@PostMapping(value = "/getEmployeeCashClaimsByDateRange", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getEmployeeCashClaimsByDateRange(@RequestHeader Map<String, String> headers,@RequestBody Map<String,String> map) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			validateRequestBodyDateRange(map);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = eccService.getEmployeeCashClaimsByDateRange(headers, map);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeCashClaimsController.getEmployeeCashClaimsByDateRange " + TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));

		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@GetMapping(value = "/getEmployeeCashClaimsById/{employeeClaimId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getEmployeeCashClaimsById(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger employeeClaimId) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = eccService.getEmployeeCashClaimsById(headers, employeeClaimId);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeCashClaimsController.getEmployeeCashClaimsById "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@PostMapping(value = "/saveEmployeeCashClaims", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveEmployeeCashClaims(@RequestHeader Map<String, String> headers,
			@RequestBody PMSEmployeeCashClaimsHdPojo req) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = eccService.saveEmployeeCashClaims(headers, req);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeCashClaimsController.saveEmployeeCashClaims "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));

		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@PutMapping(value = "/editEmployeeCashClaims/{employeeClaimId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> editEmployeeCashClaims(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger employeeClaimId, @RequestBody PMSEmployeeCashClaimsHdPojo req) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);

			if (!employeeClaimId.equals(req.getEmployeeId())) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Employee Vendor ID(Empoyee Vendor Master) Is Not Matched With Path Value ");
			}

			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = eccService.editEmployeeCashClaims(headers, req);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeCashClaimsController.editEmployeeCashClaims "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@PostMapping(value = "/submitEmployeeCashClaimsConfirmation", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> submitEmployeeCashClaimsConfirmation(@RequestHeader Map<String, String> headers,
			@RequestBody Map<String, String> bodyMap) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = eccService.updateEmployeeCashClaimsSubmitStatus(headers, bodyMap);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeCashClaimsController.updateEmployeeCashClaimsSubmitStatus "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
/*
	
	
	
//
	

	@GetMapping(value = "/getMasterPriceListByNo/{supplierNo}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getRequisitionByRequisitionNo(@RequestHeader Map<String, String> headers,
			@PathVariable String supplierNo) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMplByNo(headers, supplierNo);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.getMasterPriceListByNo "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}


	@PutMapping(value = "/changeMasterPriceListStatus/{supplierId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> changeRequisitionStatus(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger supplierId, @RequestBody Map<String, String> status) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);

			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = mplService.changeRequisitionStatus(headers, supplierId);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.changeMasterPriceListStatus "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}

	

	
	@GetMapping(value = "/getContractSupplierVendor/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getContractSupplierVendor(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger categoryId) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getContractSupplierVendor(headers, categoryId);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.getContractSupplierVendor "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	
	@PostMapping(value = "/getMasterPriceListVendors", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getContractSupplierVendor(@RequestHeader Map<String, String> headers
			,@RequestBody Map<String,String> map) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMasterPriceListVendors(headers, map);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.getMasterPriceListVendors "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
*/
}
