package com.pms.sentek.controller;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pms.common.controller.BaseController;
import com.pms.common.exception.HeaderException;
import com.pms.common.pojo.PMSEmployeeVendorClaimsHdPojo;

@RestController
@RequestMapping("/employeeVendorClaims")
public class PMSEmployeeVendorClaimsController extends BaseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSEmployeeVendorClaimsController.class);

	@Autowired
	com.pms.sentek.service.PMSEmployeeVendorClaimsService evcService;
	
	@PostMapping(value = "/getEmployeeVendorClaimsByDateRange", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getEmployeeVendorClaimsByDateRange(@RequestHeader Map<String, String> headers,@RequestBody Map<String,String> map) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			validateRequestBodyDateRange(map);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = evcService.getEmployeeVendorClaimsByDateRange(headers, map);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeVendorClaimsController.getEmployeeVendorClaimsByDateRange " + TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));

		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@GetMapping(value = "/getEmployeeVendorClaimsById/{empVendorClaimId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getEmployeeVendorClaimsById(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger empVendorClaimId) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = evcService.getEmployeeVendorClaimsById(headers, empVendorClaimId);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeVendorClaimsController.getEmployeeVendorClaimsById "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@PostMapping(value = "/saveEmployeeVendorClaims", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveEmployeeVendorClaims(@RequestHeader Map<String, String> headers,
			@RequestBody PMSEmployeeVendorClaimsHdPojo req) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = evcService.saveEmployeeVendorClaims(headers, req);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeVendorClaimsController.saveEmployeeVendorClaims "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));

		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@PutMapping(value = "/editEmployeeVendorClaims/{empVendorClaimId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> editEmployeeVendorClaims(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger empVendorClaimId, @RequestBody PMSEmployeeVendorClaimsHdPojo req) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);

			if (!empVendorClaimId.equals(req.getEmpVendorClaimId())) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Employee Vendor ID(Empoyee Vendor Master) Is Not Matched With Path Value ");
			}

			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = evcService.editEmployeeVendorClaims(headers, req);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeVendorClaimsController.editEmployeeVendorClaims "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@PutMapping(value = "/changeEmployeeVendorClaimsStatus/{empVendorClaimId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> changeEmployeeVendorClaimsStatus(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger empVendorClaimId, @RequestBody Map<String, String> status) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);

			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = evcService.changeRequisitionStatus(headers, empVendorClaimId);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeVendorClaimsController.changeEmployeeVendorClaimsStatus "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@PostMapping(value = "/updateEmployeeVendorClaimsPostingInfo", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateEmployeeVendorClaimsPostingInfo(@RequestHeader Map<String, String> headers,
			@RequestBody Map<String, String> bodyMap) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = evcService.updateEmployeeVendorClaimsPostingInfo(headers, bodyMap);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeVendorClaimsController.updateEmployeeVendorClaimsPostingInfo "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@PostMapping(value = "/submitEmployeeVendorClaimsConfirmation", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> submitEmployeeVendorClaimsConfirmation(@RequestHeader Map<String, String> headers,
			@RequestBody Map<String, String> bodyMap) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = evcService.updateEmployeeVendorClaimsSubmitStatus(headers, bodyMap);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSEmployeeVendorClaimsController.updateEmployeeVendorClaimsSubmitStatus "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
/*
	
	
	
//
	

	@GetMapping(value = "/getMasterPriceListByNo/{supplierNo}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getRequisitionByRequisitionNo(@RequestHeader Map<String, String> headers,
			@PathVariable String supplierNo) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMplByNo(headers, supplierNo);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.getMasterPriceListByNo "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}


	

	

	
	@GetMapping(value = "/getContractSupplierVendor/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getContractSupplierVendor(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger categoryId) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getContractSupplierVendor(headers, categoryId);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.getContractSupplierVendor "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	
	@PostMapping(value = "/getMasterPriceListVendors", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getContractSupplierVendor(@RequestHeader Map<String, String> headers
			,@RequestBody Map<String,String> map) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMasterPriceListVendors(headers, map);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.getMasterPriceListVendors "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
*/
}
