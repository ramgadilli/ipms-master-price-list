package com.pms.sentek.repository;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.pms.common.entity.PMSEmployeeVendorClaimsHd;
import com.pms.common.entity.PMSEmployeeVendorClaimsHdPk;

public interface PMSEmployeeVendorClaimsHdRepository
		extends JpaRepository<PMSEmployeeVendorClaimsHd, PMSEmployeeVendorClaimsHdPk> {
	
	@Query("select q from PMSEmployeeVendorClaimsHd q  where q.trxDate between :startDate and :endDate "
			+ " and q.vendorId.vendorId = Case when :vendorId > java.math.BigInteger.ZERO then :vendorId else q.vendorId.vendorId end  and q.processStatusDesc = Case when :processStatusDesc != '' then :processStatusDesc else q.processStatusDesc end    order by q.pk.empVendorClaimId asc")
	List<PMSEmployeeVendorClaimsHd> findAllEmployeeVendorClaimsByDateBetween(Date startDate, Date endDate, BigInteger vendorId, String processStatusDesc);
	
	@Query("SELECT e from PMSEmployeeVendorClaimsHd e where e.pk.empVendorClaimId=:empVendorClaimId order by e.pk.empVendorClaimId asc")
	Optional<PMSEmployeeVendorClaimsHd> fineEmployeeVendorClaimsById(BigInteger empVendorClaimId);
	
	@Modifying
	@Transactional
	@Query("UPDATE PMSEmployeeVendorClaimsHd SET isSubmitted = :isSubmitted, submittedBy = :submittedBy, submittedDate =:submittedDate WHERE  transactionId=:transactionId and moduleId=:moduleId and empVendorClaimId=:empVendorClaimId ")
	public int updateEmployeeVendorClaimsPostingInfo(@Param("isSubmitted") int isSubmitted, @Param("submittedBy") String submittedBy,
			@Param("submittedDate") Timestamp submittedDate, @Param("transactionId") int transactionId,
			@Param("moduleId") int moduleId, @Param("empVendorClaimId") BigInteger empVendorClaimId);
	
	@Modifying
	@Transactional
	@Query("UPDATE PMSEmployeeVendorClaimsHd SET isSubmitted = :isSubmitted, submittedBy = :submittedBy, submittedDate =:submittedDate WHERE  pk.empVendorClaimId=:empVendorClaimId ")
	public int updateEmployeeVendorClaimsSubmitStatus(@Param("isSubmitted") int isSubmitted, @Param("submittedBy") String submittedBy,
			@Param("submittedDate") Timestamp submittedDate, @Param("empVendorClaimId") BigInteger empVendorClaimId);
	
	//@Query("SELECT e from PMSEmployeeVendorClaimsHd e where e.pk.empVendorClaimId=:empVendorClaimId and e.admVendorId.vendorId=:vendorId   order by e.pk.empVendorClaimId asc")
	//Optional<PMSEmployeeVendorClaimsHd> findMplEmpVendorClaimIdAndVendorId(BigInteger empVendorClaimId, BigInteger vendorId);
	
	//@Query("SELECT e from PMSEmployeeVendorClaimsHd e where e.pk.empVendorClaimId=:empVendorClaimId   order by e.pk.empVendorClaimId asc")
	//Optional<PMSEmployeeVendorClaimsHd> findEmpVendorClaimNoById(BigInteger empVendorClaimId);
	
	//@Query("SELECT e from PMSEmployeeVendorClaimsHd e where e.pk.empVendorClaimNo=:empVendorClaimNo and e.admBarge.bargeId in (0,:bargeId)   order by e.pk.empVendorClaimId asc")
	//Optional<PMSEmployeeVendorClaimsHd> findEmpVendorClaimNoAndBargeId(String empVendorClaimNo, BigInteger bargeId);
	
	//@Query("SELECT e from PMSEmployeeVendorClaimsHd e where e.pk.empVendorClaimNo=:empVendorClaimNo and e.admVendorId.vendorId=:vendorId   order by e.pk.empVendorClaimId asc")
	//Optional<PMSEmployeeVendorClaimsHd> findEmpVendorClaimNoAndVendorId(String empVendorClaimNo, BigInteger vendorId);
	
	//@Query("SELECT e from PMSEmployeeVendorClaimsHd e where e.pk.empVendorClaimNo=:empVendorClaimNo   order by e.pk.empVendorClaimId asc")
	//Optional<PMSEmployeeVendorClaimsHd> findEmpVendorClaimByNo(String empVendorClaimNo);


}
