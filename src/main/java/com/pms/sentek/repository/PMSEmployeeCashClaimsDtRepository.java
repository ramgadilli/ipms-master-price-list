package com.pms.sentek.repository;

import java.math.BigInteger;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pms.common.entity.PMSContractSupplierPriceDt;
import com.pms.common.entity.PMSContractSupplierPriceDtPk;
import com.pms.common.entity.PMSEmployeeCashClaimsDt;
import com.pms.common.entity.PMSEmployeeCashClaimsDtPk;
import com.pms.sentek.service.transaction.PMSEmployeeCashClaimsServiceTransaction;;

public interface PMSEmployeeCashClaimsDtRepository
		extends JpaRepository<PMSEmployeeCashClaimsDt, PMSEmployeeCashClaimsDtPk> {
	
	
	@Transactional
	@Modifying
	@Query("delete from PMSEmployeeCashClaimsDt q where q.pk.employeeClaimId=:employeeClaimId")
	void deleteByEmployeeClaimId(@Param("employeeClaimId") BigInteger employeeClaimId);


}
