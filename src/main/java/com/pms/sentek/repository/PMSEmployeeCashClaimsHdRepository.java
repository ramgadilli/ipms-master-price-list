package com.pms.sentek.repository;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.pms.common.entity.PMSEmployeeCashClaimsHd;
import com.pms.common.entity.PMSEmployeeCashClaimsHdPk;

public interface PMSEmployeeCashClaimsHdRepository
		extends JpaRepository<PMSEmployeeCashClaimsHd, PMSEmployeeCashClaimsHdPk> {
	
	@Query("select q from PMSEmployeeCashClaimsHd q  where q.trxDate between :startDate and :endDate  "
			+ " and q.employeeId.employeeId = Case when :employeeId > java.math.BigInteger.ZERO then :employeeId else q.employeeId.employeeId end  and q.processStatusDesc = Case when :processStatusDesc != '' then :processStatusDesc else q.processStatusDesc end    order by q.pk.employeeClaimId asc")
	List<PMSEmployeeCashClaimsHd> findAllEmployeeCashClaimsByDateBetween(Date startDate, Date endDate, BigInteger employeeId, String processStatusDesc);
	
	@Query("SELECT e from PMSEmployeeCashClaimsHd e where e.pk.employeeClaimId=:employeeClaimId order by e.pk.employeeClaimId asc")
	Optional<PMSEmployeeCashClaimsHd> fineEmployeeCashClaimsById(BigInteger employeeClaimId);
	
	@Modifying
	@Transactional
	@Query("UPDATE PMSEmployeeCashClaimsHd SET isSubmitted = :isSubmitted, submittedBy = :submittedBy, submittedDate =:submittedDate WHERE  pk.employeeClaimId=:employeeClaimId ")
	public int updateEmployeeCashClaimsSubmitStatus(@Param("isSubmitted") int isSubmitted, @Param("submittedBy") String submittedBy,
			@Param("submittedDate") Timestamp submittedDate, @Param("employeeClaimId") BigInteger employeeClaimId);
	
	//@Query("SELECT e from PMSEmployeeCashClaimsHd e where e.pk.empVendorClaimId=:empVendorClaimId and e.admVendorId.vendorId=:vendorId   order by e.pk.empVendorClaimId asc")
	//Optional<PMSEmployeeCashClaimsHd> findMplEmpVendorClaimIdAndVendorId(BigInteger empVendorClaimId, BigInteger vendorId);
	
	//@Query("SELECT e from PMSEmployeeCashClaimsHd e where e.pk.empVendorClaimId=:empVendorClaimId   order by e.pk.empVendorClaimId asc")
	//Optional<PMSEmployeeCashClaimsHd> findEmpVendorClaimNoById(BigInteger empVendorClaimId);
	
	//@Query("SELECT e from PMSEmployeeCashClaimsHd e where e.pk.empVendorClaimNo=:empVendorClaimNo and e.admBarge.bargeId in (0,:bargeId)   order by e.pk.empVendorClaimId asc")
	//Optional<PMSEmployeeCashClaimsHd> findEmpVendorClaimNoAndBargeId(String empVendorClaimNo, BigInteger bargeId);
	
	//@Query("SELECT e from PMSEmployeeCashClaimsHd e where e.pk.empVendorClaimNo=:empVendorClaimNo and e.admVendorId.vendorId=:vendorId   order by e.pk.empVendorClaimId asc")
	//Optional<PMSEmployeeCashClaimsHd> findEmpVendorClaimNoAndVendorId(String empVendorClaimNo, BigInteger vendorId);
	
	//@Query("SELECT e from PMSEmployeeCashClaimsHd e where e.pk.empVendorClaimNo=:empVendorClaimNo   order by e.pk.empVendorClaimId asc")
	//Optional<PMSEmployeeCashClaimsHd> findEmpVendorClaimByNo(String empVendorClaimNo);


}
