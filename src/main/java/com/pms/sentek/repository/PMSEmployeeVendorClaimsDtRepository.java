package com.pms.sentek.repository;

import java.math.BigInteger;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pms.common.entity.PMSContractSupplierPriceDt;
import com.pms.common.entity.PMSContractSupplierPriceDtPk;
import com.pms.common.entity.PMSEmployeeVendorClaimsDt;
import com.pms.common.entity.PMSEmployeeVendorClaimsDtPk;
import com.pms.sentek.service.transaction.PMSEmployeeVendorClaimsServiceTransaction;;

public interface PMSEmployeeVendorClaimsDtRepository
		extends JpaRepository<PMSEmployeeVendorClaimsDt, PMSEmployeeVendorClaimsDtPk> {
	
	
	@Transactional
	@Modifying
	@Query("delete from PMSEmployeeVendorClaimsDt q where q.pk.empVendorClaimId=:empVendorClaimId")
	void deleteByEmpVendorClaimId(@Param("empVendorClaimId") BigInteger empVendorClaimId);


}
