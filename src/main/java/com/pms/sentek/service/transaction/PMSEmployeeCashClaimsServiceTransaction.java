package com.pms.sentek.service.transaction;

import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.pms.common.constants.CommonConstants;
import com.pms.common.entity.AdmBarge;
import com.pms.common.entity.AdmEmployee;
import com.pms.common.entity.AdmGST;
import com.pms.common.entity.AdmIMPACode;
import com.pms.common.entity.AdmMainCode;
import com.pms.common.entity.AdmPMSStatus;
import com.pms.common.entity.AdmUOM;
import com.pms.common.entity.AdmVendor;
import com.pms.common.entity.PMSEmployeeCashClaimsDt;
import com.pms.common.entity.PMSEmployeeCashClaimsDtPk;
import com.pms.common.entity.PMSEmployeeCashClaimsHd;
import com.pms.common.entity.PMSEmployeeCashClaimsHdPk;
import com.pms.common.pojo.MplVendorPojo;
import com.pms.common.pojo.PMSEmployeeCashClaimsDtPojo;
import com.pms.common.pojo.PMSEmployeeCashClaimsHdPojo;
import com.pms.common.pojo.SupplierVendorPojo;
import com.pms.common.service.transaction.BaseServiceTrans;
import com.pms.common.utilities.DateUtilities;
import com.pms.common.utilities.Utilities;
import com.pms.sentek.repository.PMSEmployeeCashClaimsDtRepository;
import com.pms.sentek.repository.PMSEmployeeCashClaimsHdRepository;

@Service
public class PMSEmployeeCashClaimsServiceTransaction {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSEmployeeCashClaimsServiceTransaction.class);

	private final NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	PMSEmployeeCashClaimsServiceTransaction(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Autowired
	PMSEmployeeCashClaimsHdRepository employeeCashClaimsHdRepository;
	@Autowired
	PMSEmployeeCashClaimsDtRepository employeeCashClaimsDtRepository;

	@Autowired
	BaseServiceTrans baseSerTrans;

	List<PMSEmployeeCashClaimsHdPojo> evcListPjo = new ArrayList<PMSEmployeeCashClaimsHdPojo>();

	PMSEmployeeCashClaimsHd employeeCashClaimsHd = new PMSEmployeeCashClaimsHd();
	PMSEmployeeCashClaimsHdPk employeeCashClaimsHdPk = new PMSEmployeeCashClaimsHdPk();

	PMSEmployeeCashClaimsDt employeeCashClaimsDt = new PMSEmployeeCashClaimsDt();
	List<PMSEmployeeCashClaimsDt> evcDtList = new ArrayList<PMSEmployeeCashClaimsDt>();
	PMSEmployeeCashClaimsDtPk employeeCashClaimsDtPk = new PMSEmployeeCashClaimsDtPk();

	private void convertEntToPojo(PMSEmployeeCashClaimsHd ent, PMSEmployeeCashClaimsHdPojo evcHdPojo,
			boolean isList) {

		BeanUtils.copyProperties(ent, evcHdPojo);
		evcHdPojo.setEmployeeClaimId(ent.getPk().getEmployeeClaimId());
		evcHdPojo.setEmployeeClaimNo(ent.getPk().getEmployeeClaimNo());
		
		
		//evcHdPojo.setEmployeeCode(ent.getEmployeeCode() );
		//evcHdPojo.setEmployeeName(ent.getEmployeeName() );
		evcHdPojo.setPmsStatusCode(ent.getAdmPMSStatus().getPmsStatusCode() );
		evcHdPojo.setPmsStatusName(ent.getAdmPMSStatus().getPmsStatusName() );

		//evcHdPojo.setVendorId(ent.getAdmVendorId().getVendorId());
		if (!isList) {
			for (PMSEmployeeCashClaimsDt evcDt : ent.getEmployeeCashClaimsDetails()) {
				PMSEmployeeCashClaimsDtPojo evcDtPojo = new PMSEmployeeCashClaimsDtPojo();
				BeanUtils.copyProperties(evcDt, evcDtPojo);
				//evcDtPojo.setSupplierContractId(evcDt.getPk().getEmployeeClaimId());
				//evcDtPojo.setSupplierContractNo(evcDt.getPk().getEmployeeClaimNo());
				//evcDtPojo.setItemNo(evcDt.getPk().getItemNo());
				evcHdPojo.addEmployeeCashClaimsDetails(evcDtPojo);
			}
		} else {
			evcHdPojo.setEmployeeCashClaimsDetails(null);
		}
	}

	@Transactional
	public List<PMSEmployeeCashClaimsHdPojo> getEmployeeCashClaimsByDateRange(Map<String, String> headers, Map<String, String> map) {
		LOGGER.info("Received getEmployeeCashClaimsByDateRange  ");
		List<PMSEmployeeCashClaimsHd> employeeCashClaimsHdList = null;
		evcListPjo = new ArrayList<PMSEmployeeCashClaimsHdPojo>();
		PMSEmployeeCashClaimsHdPojo pojo = null;
		
		Date startDate = null;
		Date endDate = null;
		//BigInteger bargeId = BigInteger.ZERO;
		//BigInteger vendorId = BigInteger.ZERO;
		BigInteger employeeId = BigInteger.ZERO;
		String processStatusDesc = ""; 
		
		try {
			
			startDate = Date.valueOf(map.get("startDate"));
			endDate = Date.valueOf(map.get("endDate"));

			employeeId = map.containsKey("employeeId") ? new BigInteger(String.valueOf(map.get("employeeId"))) : BigInteger.ZERO;
			//bargeId = map.containsKey("bargeId") ? new BigInteger(String.valueOf(map.get("bargeId"))) : BigInteger.ZERO;
			//vendorId = map.containsKey("vendorId") ? new BigInteger(String.valueOf(map.get("vendorId"))) : BigInteger.ZERO;
			processStatusDesc = map.containsKey("processStatusDesc") ? (String)map.get("processStatusDesc") : "";
			processStatusDesc = processStatusDesc == null ? "" : processStatusDesc;
			java.sql.Date futureDate = Utilities.addDays(endDate, 1);
			
			employeeCashClaimsHdList = employeeCashClaimsHdRepository.findAllEmployeeCashClaimsByDateBetween(startDate, futureDate, employeeId, processStatusDesc);

			if (employeeCashClaimsHdList != null && employeeCashClaimsHdList.size() > 0) {
				for (PMSEmployeeCashClaimsHd employeeList : employeeCashClaimsHdList) {
					pojo = new PMSEmployeeCashClaimsHdPojo();
					convertEntToPojo(employeeList, pojo, false);
					evcListPjo.add(pojo);
				}

			}

		} catch (Exception ex) {
			throw ex;
		}
		return evcListPjo;
	}
	
	public Optional<PMSEmployeeCashClaimsHd> getEmployeeCashClaimsById(Map<String, String> headers, BigInteger employeeClaimId) {
		Optional<PMSEmployeeCashClaimsHd> hd = null;
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		try {
			LOGGER.info("getEmployeeCashClaimsById for Employee Claims ID: " + employeeClaimId);

			if (roleId == 1) {
				hd = employeeCashClaimsHdRepository.fineEmployeeCashClaimsById(employeeClaimId);
			} /*
				 * else if (roleId == 2) { hd =
				 * employeeCashClaimsHdRepository.findMplSupplierContractIdAndBargeId(
				 * employeeClaimId, ownerId); } else if (roleId == 3) { hd =
				 * employeeCashClaimsHdRepository.findMplSupplierContractIdAndVendorId(
				 * employeeClaimId, ownerId); }
				 */

		} catch (Exception e) {
			throw e;
		}
		return hd;
	}
	
	@Transactional
	public PMSEmployeeCashClaimsHdPojo saveEmployeeCashClaims(Map<String, String> headers, PMSEmployeeCashClaimsHdPojo req)
			throws Exception {

		boolean transactionSuccess = false;
		Map<String, Object> map = null;
		BigInteger outNewBtsId = null;
		String outNewDocCode = null;
		employeeCashClaimsHd = new PMSEmployeeCashClaimsHd();
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));

		try {
			Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());
			map = baseSerTrans.getGeneratedSeqNo(req.getBranchId(), moduleId, transactionId,
					CommonConstants.BANK_ID_ZERO, currentDate, CommonConstants.IN_PREFIX, 0);
			outNewBtsId = new BigInteger(map.get("outNewBtsId").toString());
			outNewDocCode = map.get("outNewDocCode").toString();
			req.setEmployeeClaimId(outNewBtsId);
			req.setEmployeeClaimNo(outNewDocCode);
			BeanUtils.copyProperties(req, employeeCashClaimsHd);

			employeeCashClaimsHdPk = new PMSEmployeeCashClaimsHdPk(req.getEmployeeClaimId(), req.getEmployeeClaimNo());
			employeeCashClaimsHd.setPk(employeeCashClaimsHdPk);
			//employeeCashClaimsHd.setAdmVendorId(new AdmVendor(req.getVendorId()));
			employeeCashClaimsHd.setCreateDate(DateUtilities.getCurrentDate());
			employeeCashClaimsHd.setCreateBy(userCode);
			employeeCashClaimsHd.setModuleId(moduleId);
			employeeCashClaimsHd.setTransactionId(transactionId);
			employeeCashClaimsHd.setBranchId(branchId);
			employeeCashClaimsHd.setAdmPMSStatus(new AdmPMSStatus(CommonConstants.PMS_STATUS_PURCHASECLAIMS));
			employeeCashClaimsHd.setEmployeeId(new AdmEmployee(req.getEmployeeId() == null ? "" : req.getEmployeeId().toString()));
			// contractSupplierPriceHd.setEditDate(currentDateTime);
			employeeCashClaimsHd.setTrxDate(DateUtilities.getCurrentDate());

			LOGGER.info("Created New Supplier Contract Id :- " + outNewBtsId + " & New Supplier Contract No :- " + outNewDocCode);

			for (PMSEmployeeCashClaimsDtPojo reqDetails : req.getEmployeeCashClaimsDetails()) {

				employeeCashClaimsDt = new PMSEmployeeCashClaimsDt();
				employeeCashClaimsDtPk = new PMSEmployeeCashClaimsDtPk(outNewBtsId, outNewDocCode, reqDetails.getItemNo());
				BeanUtils.copyProperties(reqDetails, employeeCashClaimsDt);
				employeeCashClaimsDt.setPk(employeeCashClaimsDtPk);
				employeeCashClaimsDt.setBranchId(branchId);
				employeeCashClaimsDt.setBargeId(new AdmBarge(reqDetails.getBargeId()));
				employeeCashClaimsDt.setiMPACodeId(new AdmIMPACode(reqDetails.getiMPACodeId() == null ? BigInteger.ZERO : reqDetails.getiMPACodeId()));
				employeeCashClaimsDt.setMainCodeId( new AdmMainCode(reqDetails.getMainCodeId() == null ? BigInteger.ZERO : reqDetails.getMainCodeId()));
				employeeCashClaimsDt.setuOMId(new AdmUOM(reqDetails.getuOMId() == null ? BigInteger.ZERO : reqDetails.getuOMId()));
				employeeCashClaimsDt.setgSTId(new AdmGST(reqDetails.getgSTId() == null ? BigInteger.ZERO : reqDetails.getgSTId()));
				
				employeeCashClaimsHd.addEmployeeCashClaimsDetails(employeeCashClaimsDt);
			}
			employeeCashClaimsHdRepository.saveAndFlush(employeeCashClaimsHd);
			transactionSuccess = true;
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.EMPLOYEE_CASH_CLAIM_TABLE_NAME, outNewDocCode, "" + req.getEmployeeClaimId(), outNewBtsId + "", "Create", req.getEmployeeClaimId() + "");

			}
		}
		return req;
	}
	
	@Transactional
	public PMSEmployeeCashClaimsHdPojo editEmployeeCashClaims(Map<String, String> headers, PMSEmployeeCashClaimsHdPojo req)
			throws Exception {

		boolean transactionSuccess = false;
		Map<String, Object> map = null;
		BigInteger outNewBtsId = null;
		String outNewDocCode = null;
		employeeCashClaimsHd = new PMSEmployeeCashClaimsHd();
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));

		evcDtList = new ArrayList<PMSEmployeeCashClaimsDt>();
		
		try {
			baseSerTrans.createHistoryRecord(req.getBranchId(), moduleId, transactionId, req.getEmployeeClaimId());
			Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());


			BeanUtils.copyProperties(req, employeeCashClaimsHd);

			employeeCashClaimsHdPk = new PMSEmployeeCashClaimsHdPk(req.getEmployeeClaimId(), req.getEmployeeClaimNo());
			employeeCashClaimsHd.setPk(employeeCashClaimsHdPk);
			//employeeCashClaimsHd.setAdmVendorId(new AdmVendor(req.getVendorId()));

			employeeCashClaimsHd.setEditDate(DateUtilities.getCurrentDate());
			employeeCashClaimsHd.setEditBy(userCode);

			LOGGER.info("Existing Employee Cash Claim Id :- " + req.getEmployeeClaimId()
					+ " & Existing Employee Cash Claim No :- " + req.getEmployeeClaimNo());

			for (PMSEmployeeCashClaimsDtPojo reqDetails : req.getEmployeeCashClaimsDetails()) {

				employeeCashClaimsDt = new PMSEmployeeCashClaimsDt();
				employeeCashClaimsDtPk = new PMSEmployeeCashClaimsDtPk(req.getEmployeeClaimId(), req.getEmployeeClaimNo(), reqDetails.getItemNo());
				BeanUtils.copyProperties(reqDetails, employeeCashClaimsDt);
				employeeCashClaimsDt.setPk(employeeCashClaimsDtPk);
				employeeCashClaimsHd.addEmployeeCashClaimsDetails(employeeCashClaimsDt);

				evcDtList.add(employeeCashClaimsDt);
			}

			//employeeCashClaimsDtRepository.deleteAll(evcDtList);
			employeeCashClaimsDtRepository.deleteByEmployeeClaimId(req.getEmployeeClaimId());
			employeeCashClaimsHdRepository.flush();
			employeeCashClaimsHdRepository.save(employeeCashClaimsHd);
//			contractSupplierPriceHdRepository.flush();
//			contractSupplierPriceDtRepository.flush();
			LOGGER.info("request to edit EVC for : " + req.getEmployeeClaimId());
			transactionSuccess = true;
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.EMPLOYEE_CASH_CLAIM_TABLE_NAME, outNewDocCode, "" + req.getEmployeeClaimId(),
						outNewBtsId + "", "Edit", req.getEmployeeClaimId() + "");

			}
		}
		return req;
	}
	
	public int updateEmployeeCashClaimsSubmitStatus(Map<String, String> headers, BigInteger employeeClaimId) {
		int hd;
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		try {
			LOGGER.info("updateEmployeeVendorClaimsSubmitStatus for EmployeeVendorClaim : " + employeeClaimId);
			hd = employeeCashClaimsHdRepository.updateEmployeeCashClaimsSubmitStatus(1, userCode, DateUtilities.getCurrentDate(), 
					employeeClaimId);

		} catch (Exception e) {
			throw e;
		}
		return hd;
	}

	/*
	

	

	

	public Optional<PMSContractSupplierPriceHd> getMplByNo(Map<String, String> headers, String supplierNo) {
		Optional<PMSContractSupplierPriceHd> hd = null;
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		try {
			LOGGER.info("getMpl for Master Price List : " + supplierNo);

			if (roleId == 1) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractByNo(supplierNo);
			} else if (roleId == 2) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractNoAndVendorId(supplierNo, ownerId);
			} else if (roleId == 3) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractNoAndVendorId(supplierNo, ownerId);
			}

		} catch (Exception e) {
			throw e;
		}
		return hd;
	}

	@Transactional
	public List<SupplierVendorPojo> getContractSupplierVendor(BigInteger categoryId) {
		String query = "select PricesHd.VendorId, Vendor.VendorCode, Vendor.VendorName from PMSContractSupplierPriceHd PricesHd, AdmVendor Vendor where PricesHd.VendorId = Vendor.VendorId"
				+ " and PricesHd.ContractCategoryId = :categoryId "
				+ "and PricesHd.ValidFrom <= getdate() and PricesHd.ExpiryDate >= getdate()";

		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("categoryId", "" + categoryId);

		List<SupplierVendorPojo> searchResults = null;
		try {
			searchResults = jdbcTemplate.query(query, queryParams,
					new BeanPropertyRowMapper<>(SupplierVendorPojo.class));

		} catch (Exception e) {
			throw e;
		}

		return searchResults;
	}

	@Transactional
	public List<MplVendorPojo> getMasterPriceListVendors(Map<String, String> headers, Map<String, String> map) {

		Date dateRange = Date.valueOf(map.get("trxDate"));
		String query = "select hd.VendorId, hd.ContactPersonId, hd.SupplierContractId, hd.SupplierContractNo, vdr.vendorName, vdr.vendorCode \r\n"
				+ "from PMSContractSupplierPriceHd hd, admVendor vdr where hd.BranchId = vdr.BranchId \r\n"
				+ "and hd.VendorId = vdr.VendorId\r\n" + "and vdr.BranchId = :branchId\r\n"
				+ "and hd.IsCancel = 0 and  hd.ContractCategoryId = :contractCategoryId and hd.BargeId in (0, :bargeId) \r\n"
				+ "and hd.ValidFrom <= :date and hd.ExpiryDate >= :date;";

		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("date", "" + dateRange);
		queryParams.put("contractCategoryId", "" + map.get("contractCategoryId"));
		queryParams.put("bargeId", "" + map.get("bargeId"));
		queryParams.put("branchId", "" + String.valueOf(headers.get(CommonConstants.BRANCH_ID)));

		List<MplVendorPojo> searchResults = null;
		try {
			searchResults = jdbcTemplate.query(query, queryParams, new BeanPropertyRowMapper<>(MplVendorPojo.class));

		} catch (Exception e) {
			throw e;
		}

		return searchResults;
	}

*/
}
