package com.pms.sentek.service.transaction;

import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.pms.common.constants.CommonConstants;
import com.pms.common.entity.AdmAddress;
import com.pms.common.entity.AdmBarge;
import com.pms.common.entity.AdmCurrency;
import com.pms.common.entity.AdmEmployee;
import com.pms.common.entity.AdmGST;
import com.pms.common.entity.AdmIMPACode;
import com.pms.common.entity.AdmMainCode;
import com.pms.common.entity.AdmPMSStatus;
import com.pms.common.entity.AdmUOM;
import com.pms.common.entity.AdmVendor;
import com.pms.common.entity.AdmVesselDepartment;
import com.pms.common.entity.PMSEmployeeVendorClaimsDt;
import com.pms.common.entity.PMSEmployeeVendorClaimsDtPk;
import com.pms.common.entity.PMSEmployeeVendorClaimsHd;
import com.pms.common.entity.PMSEmployeeVendorClaimsHdPk;
import com.pms.common.pojo.MplVendorPojo;
import com.pms.common.pojo.PMSEmployeeVendorClaimsDtPojo;
import com.pms.common.pojo.PMSEmployeeVendorClaimsHdPojo;
import com.pms.common.pojo.SupplierVendorPojo;
import com.pms.common.service.transaction.BaseServiceTrans;
import com.pms.common.utilities.DateUtilities;
import com.pms.common.utilities.Utilities;
import com.pms.sentek.repository.PMSEmployeeVendorClaimsDtRepository;
import com.pms.sentek.repository.PMSEmployeeVendorClaimsHdRepository;

@Service
public class PMSEmployeeVendorClaimsServiceTransaction {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSEmployeeVendorClaimsServiceTransaction.class);

	private final NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	PMSEmployeeVendorClaimsServiceTransaction(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Autowired
	PMSEmployeeVendorClaimsHdRepository employeeVendorClaimsHdRepository;
	@Autowired
	PMSEmployeeVendorClaimsDtRepository employeeVendorClaimsDtRepository;

	@Autowired
	BaseServiceTrans baseSerTrans;

	List<PMSEmployeeVendorClaimsHdPojo> evcListPjo = new ArrayList<PMSEmployeeVendorClaimsHdPojo>();

	PMSEmployeeVendorClaimsHd employeeVendorClaimsHd = new PMSEmployeeVendorClaimsHd();
	PMSEmployeeVendorClaimsHdPk employeeVendorClaimsHdPk = new PMSEmployeeVendorClaimsHdPk();

	PMSEmployeeVendorClaimsDt employeeVendorClaimsDt = new PMSEmployeeVendorClaimsDt();
	List<PMSEmployeeVendorClaimsDt> evcDtList = new ArrayList<PMSEmployeeVendorClaimsDt>();
	PMSEmployeeVendorClaimsDtPk employeeVendorClaimsDtPk = new PMSEmployeeVendorClaimsDtPk();

	private void convertEntToPojo(PMSEmployeeVendorClaimsHd ent, PMSEmployeeVendorClaimsHdPojo evcHdPojo,
			boolean isList) {

		BeanUtils.copyProperties(ent, evcHdPojo);
		evcHdPojo.setEmpVendorClaimId(ent.getPk().getEmpVendorClaimId());
		evcHdPojo.setEmpVendorClaimNo(ent.getPk().getEmpVendorClaimNo());

		evcHdPojo.setVendorId(ent.getVendorId().getVendorId());
		evcHdPojo.setVendorCode(ent.getVendorId().getVendorCode());
		evcHdPojo.setVendorName(ent.getVendorId().getVendorName());
		evcHdPojo.setContact(ent.getContactPersonId().getContact() );
		//evcHdPojo.setEmployeeCode(ent.getEmployeeCode() );
		//evcHdPojo.setEmployeeName(ent.getEmployeeName() );
		evcHdPojo.setContactPersonId(ent.getContactPersonId().getAddressId() );
		evcHdPojo.setContact(ent.getContactPersonId().getContact() );
		evcHdPojo.setEmployeeId(ent.getEmployeeId().getEmployeeId() );
		evcHdPojo.setEmployeeCode(ent.getEmployeeId().getEmployeeCode() );
		evcHdPojo.setEmployeeName(ent.getEmployeeId().getEmployeeName() );
		evcHdPojo.setCurrencyCode(ent.getCurrencyId().getCurrencyCode() );
		evcHdPojo.setCurrencyName(ent.getCurrencyId().getCurrencyName() );
		evcHdPojo.setPmsStatusCode(ent.getAdmPMSStatus().getPmsStatusCode() );
		evcHdPojo.setPmsStatusName(ent.getAdmPMSStatus().getPmsStatusName() );
		evcHdPojo.setCurrencyId(ent.getCurrencyId().getCurrencyId() );
		evcHdPojo.setStatusId(ent.getAdmPMSStatus().getPmsStatusId() );
		
		if (!isList) {
			for (PMSEmployeeVendorClaimsDt evcDt : ent.getEmployeeVendorClaimsDetails()) {
				PMSEmployeeVendorClaimsDtPojo evcDtPojo = new PMSEmployeeVendorClaimsDtPojo();
				BeanUtils.copyProperties(evcDt, evcDtPojo);
				//evcDtPojo.setSupplierContractId(evcDt.getPk().getEmpVendorClaimId());
				//evcDtPojo.setSupplierContractNo(evcDt.getPk().getEmpVendorClaimNo());
				//evcDtPojo.setItemNo(evcDt.getPk().getItemNo());
				evcHdPojo.addEmployeeVendorClaimsDetails(evcDtPojo);
			}
		} else {
			evcHdPojo.setEmployeeVendorClaimsDetails(null);
		}
	}

	@Transactional
	public List<PMSEmployeeVendorClaimsHdPojo> getEmployeeVendorClaimsByDateRange(Map<String, String> headers, Map<String, String> map) {
		LOGGER.info("Received getEmployeeVendorClaimsByDateRange  ");
		List<PMSEmployeeVendorClaimsHd> employeeVendorClaimsHdList = null;
		evcListPjo = new ArrayList<PMSEmployeeVendorClaimsHdPojo>();
		PMSEmployeeVendorClaimsHdPojo pojo = null;
		
		Date startDate = null;
		Date endDate = null;
		//BigInteger bargeId = BigInteger.ZERO;
		BigInteger vendorId = BigInteger.ZERO;
		//BigInteger employeeId = BigInteger.ZERO;
		String processStatusDesc = ""; 
		
		try {
			startDate = Date.valueOf(map.get("startDate"));
			endDate = Date.valueOf(map.get("endDate"));

			//employeeId = map.containsKey("employeeId") ? new BigInteger(String.valueOf(map.get("employeeId"))) : BigInteger.ZERO;
			//bargeId = map.containsKey("bargeId") ? new BigInteger(String.valueOf(map.get("bargeId"))) : BigInteger.ZERO;
			vendorId = map.containsKey("vendorId") ? new BigInteger(String.valueOf(map.get("vendorId"))) : BigInteger.ZERO;
			processStatusDesc = map.containsKey("processStatusDesc") ? (String)map.get("processStatusDesc") : "";
			processStatusDesc = processStatusDesc == null ? "" : processStatusDesc;
			java.sql.Date futureDate = Utilities.addDays(endDate, 1);
			
			employeeVendorClaimsHdList = employeeVendorClaimsHdRepository.findAllEmployeeVendorClaimsByDateBetween(startDate, futureDate, vendorId, processStatusDesc);

			if (employeeVendorClaimsHdList != null && employeeVendorClaimsHdList.size() > 0) {
				for (PMSEmployeeVendorClaimsHd contractSupplierList : employeeVendorClaimsHdList) {
					pojo = new PMSEmployeeVendorClaimsHdPojo();
					convertEntToPojo(contractSupplierList, pojo, false);
					evcListPjo.add(pojo);
				}

			}

		} catch (Exception ex) {
			throw ex;
		}
		return evcListPjo;
	}
	
	public Optional<PMSEmployeeVendorClaimsHd> getEmployeeVendorClaimsById(Map<String, String> headers, BigInteger empVendorClaimId) {
		Optional<PMSEmployeeVendorClaimsHd> hd = null;
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		try {
			LOGGER.info("getEmployeeVendorClaimsById for Employee Vendor Claims: " + empVendorClaimId);

			if (roleId == 1) {
				hd = employeeVendorClaimsHdRepository.fineEmployeeVendorClaimsById(empVendorClaimId);
			} /*
				 * else if (roleId == 2) { hd =
				 * employeeVendorClaimsHdRepository.findMplSupplierContractIdAndBargeId(
				 * empVendorClaimId, ownerId); } else if (roleId == 3) { hd =
				 * employeeVendorClaimsHdRepository.findMplSupplierContractIdAndVendorId(
				 * empVendorClaimId, ownerId); }
				 */

		} catch (Exception e) {
			throw e;
		}
		return hd;
	}
	
	@Transactional
	public PMSEmployeeVendorClaimsHdPojo saveEmployeeVendorClaims(Map<String, String> headers, PMSEmployeeVendorClaimsHdPojo req)
			throws Exception {

		boolean transactionSuccess = false;
		Map<String, Object> map = null;
		BigInteger outNewBtsId = null;
		String outNewDocCode = null;
		employeeVendorClaimsHd = new PMSEmployeeVendorClaimsHd();
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));

		try {
			Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());
			map = baseSerTrans.getGeneratedSeqNo(req.getBranchId(), moduleId, transactionId, CommonConstants.BANK_ID_ZERO, currentDate, CommonConstants.IN_PREFIX, 0);
			outNewBtsId = new BigInteger(map.get("outNewBtsId").toString());
			outNewDocCode = map.get("outNewDocCode").toString();
			req.setEmpVendorClaimId(outNewBtsId);
			req.setEmpVendorClaimNo(outNewDocCode);
			BeanUtils.copyProperties(req, employeeVendorClaimsHd);

			employeeVendorClaimsHdPk = new PMSEmployeeVendorClaimsHdPk(req.getEmpVendorClaimId(), req.getEmpVendorClaimNo());
			employeeVendorClaimsHd.setPk(employeeVendorClaimsHdPk);
			employeeVendorClaimsHd.setVendorId(new AdmVendor(req.getVendorId()));
			employeeVendorClaimsHd.setCurrencyId(new AdmCurrency(req.getCurrencyId()));
			employeeVendorClaimsHd.setContactPersonId(new AdmAddress(req.getContactPersonId()));
			employeeVendorClaimsHd.setEmployeeId(new AdmEmployee(req.getEmployeeId() == null ? "" : req.getEmployeeId().toString()));
			employeeVendorClaimsHd.setAdmPMSStatus(new AdmPMSStatus(CommonConstants.PMS_STATUS_PURCHASECLAIMS));
			employeeVendorClaimsHd.setCreateDate(DateUtilities.getCurrentDate());
			employeeVendorClaimsHd.setCreateBy(userCode);
			employeeVendorClaimsHd.setModuleId(moduleId);
			employeeVendorClaimsHd.setTransactionId(transactionId);
			employeeVendorClaimsHd.setBranchId(branchId);
			
			// contractSupplierPriceHd.setEditDate(currentDateTime);
			employeeVendorClaimsHd.setTrxDate(DateUtilities.getCurrentDate());

			LOGGER.info("Created New Supplier Contract Id :- " + outNewBtsId + " & New Supplier Contract No :- " + outNewDocCode);

			for (PMSEmployeeVendorClaimsDtPojo reqDetails : req.getEmployeeVendorClaimsDetails()) {

				employeeVendorClaimsDt = new PMSEmployeeVendorClaimsDt();
				employeeVendorClaimsDtPk = new PMSEmployeeVendorClaimsDtPk(outNewBtsId, outNewDocCode, reqDetails.getItemNo());
				BeanUtils.copyProperties(reqDetails, employeeVendorClaimsDt);
				employeeVendorClaimsDt.setPk(employeeVendorClaimsDtPk);
				employeeVendorClaimsDt.setBranchId(branchId);
				employeeVendorClaimsDt.setBargeId(new AdmBarge(reqDetails.getBargeId()));
				employeeVendorClaimsDt.setVesselDepartmentId(new AdmVesselDepartment(reqDetails.getVesselDepartmentId()));
				employeeVendorClaimsDt.setiMPACodeId(new AdmIMPACode(reqDetails.getiMPACodeId() == null ? BigInteger.ZERO : reqDetails.getiMPACodeId()));
				employeeVendorClaimsDt.setMainCodeId( new AdmMainCode(reqDetails.getMainCodeId() == null ? BigInteger.ZERO : reqDetails.getMainCodeId()));
				employeeVendorClaimsDt.setuOMId(new AdmUOM(reqDetails.getuOMId() == null ? BigInteger.ZERO : reqDetails.getuOMId()));
				employeeVendorClaimsDt.setgSTId(new AdmGST(reqDetails.getgSTId() == null ? BigInteger.ZERO : reqDetails.getgSTId()));
				employeeVendorClaimsHd.addemployeeVendorClaimsDetails(employeeVendorClaimsDt);
			}
			employeeVendorClaimsHdRepository.saveAndFlush(employeeVendorClaimsHd);
			transactionSuccess = true;
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.EMPLOYEE_VENDOR_CLAIM_TABLE_NAME, outNewDocCode, "" + req.getVendorId(), outNewBtsId + "", "Create", req.getVendorId() + "");

			}
		}
		return req;
	}
	
	@Transactional
	public PMSEmployeeVendorClaimsHdPojo editEmployeeVendorClaims(Map<String, String> headers, PMSEmployeeVendorClaimsHdPojo req)
			throws Exception {

		boolean transactionSuccess = false;
		Map<String, Object> map = null;
		BigInteger outNewBtsId = null;
		String outNewDocCode = null;
		employeeVendorClaimsHd = new PMSEmployeeVendorClaimsHd();
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));

		evcDtList = new ArrayList<PMSEmployeeVendorClaimsDt>();
		
		try {
			baseSerTrans.createHistoryRecord(req.getBranchId(), moduleId, transactionId, req.getEmpVendorClaimId());
			Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());


			BeanUtils.copyProperties(req, employeeVendorClaimsHd);

			employeeVendorClaimsHdPk = new PMSEmployeeVendorClaimsHdPk(req.getEmpVendorClaimId(), req.getEmpVendorClaimNo());
			employeeVendorClaimsHd.setPk(employeeVendorClaimsHdPk);
			//employeeVendorClaimsHd.setVendorId(new AdmVendor(req.getVendorId()));
			employeeVendorClaimsHd.setVendorId(new AdmVendor(req.getVendorId()));
			employeeVendorClaimsHd.setCurrencyId(new AdmCurrency(req.getCurrencyId()));
			employeeVendorClaimsHd.setContactPersonId(new AdmAddress(req.getContactPersonId()));
			employeeVendorClaimsHd.setEmployeeId(new AdmEmployee(req.getEmployeeId() == null ? "" : req.getEmployeeId().toString()));
			employeeVendorClaimsHd.setAdmPMSStatus(new AdmPMSStatus(CommonConstants.PMS_STATUS_PURCHASECLAIMS));
			employeeVendorClaimsHd.setEditDate(DateUtilities.getCurrentDate());
			employeeVendorClaimsHd.setEditBy(userCode);

			LOGGER.info("Existing Employee Vendor Claim Id :- " + req.getEmpVendorClaimId()
					+ " & Existing Employee Vendor Claim No :- " + req.getEmpVendorClaimNo());

			for (PMSEmployeeVendorClaimsDtPojo reqDetails : req.getEmployeeVendorClaimsDetails()) {

				employeeVendorClaimsDt = new PMSEmployeeVendorClaimsDt();
				employeeVendorClaimsDtPk = new PMSEmployeeVendorClaimsDtPk(req.getEmpVendorClaimId(), req.getEmpVendorClaimNo(), reqDetails.getItemNo());
				BeanUtils.copyProperties(reqDetails, employeeVendorClaimsDt);
				employeeVendorClaimsDt.setPk(employeeVendorClaimsDtPk);
				
				employeeVendorClaimsDt.setBargeId(new AdmBarge(reqDetails.getBargeId()));
				employeeVendorClaimsDt.setVesselDepartmentId(new AdmVesselDepartment(reqDetails.getVesselDepartmentId()));
				employeeVendorClaimsDt.setiMPACodeId(new AdmIMPACode(reqDetails.getiMPACodeId() == null ? BigInteger.ZERO : reqDetails.getiMPACodeId()));
				employeeVendorClaimsDt.setMainCodeId( new AdmMainCode(reqDetails.getMainCodeId() == null ? BigInteger.ZERO : reqDetails.getMainCodeId()));
				employeeVendorClaimsDt.setuOMId(new AdmUOM(reqDetails.getuOMId() == null ? BigInteger.ZERO : reqDetails.getuOMId()));
				employeeVendorClaimsDt.setgSTId(new AdmGST(reqDetails.getgSTId() == null ? BigInteger.ZERO : reqDetails.getgSTId()));
				
				employeeVendorClaimsHd.addemployeeVendorClaimsDetails(employeeVendorClaimsDt);

				evcDtList.add(employeeVendorClaimsDt);
			}

			//employeeVendorClaimsDtRepository.deleteAll(evcDtList);
			employeeVendorClaimsDtRepository.deleteByEmpVendorClaimId(req.getEmpVendorClaimId());
			employeeVendorClaimsHdRepository.flush();
			employeeVendorClaimsHdRepository.save(employeeVendorClaimsHd);
//			contractSupplierPriceHdRepository.flush();
//			contractSupplierPriceDtRepository.flush();
			LOGGER.info("request to edit EVC for : " + req.getVendorId());
			transactionSuccess = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.EMPLOYEE_VENDOR_CLAIM_TABLE_NAME, outNewDocCode, "" + req.getVendorId(),
						outNewBtsId + "", "Edit", req.getVendorId() + "");

			}
		}
		return req;
	}
	
	public int updateEmployeeVendorClaimsPostingInfo(Map<String, String> headers, BigInteger empVendorClaimId)  {
		int hd = 0;
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		String insertRecordOperation = "createNewVersionedRecord";
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		try {
			LOGGER.info("updateStatus for DeliveryOrder : " + empVendorClaimId);
			hd = employeeVendorClaimsHdRepository.updateEmployeeVendorClaimsPostingInfo(1, userCode, DateUtilities.getCurrentDate(), transactionId, moduleId, empVendorClaimId);
			//baseSerTrans.updateProcessStatusDescInHD( branchId,  moduleId,  transactionId, empVendorClaimId,  CommonConstants.EMPLOYEE_VENDOR_CLAIM_TABLE_NAME,  "VendorClaims");
			
		} catch (Exception e) {
			LOGGER.error("Received Error "+e.getMessage());
		}
		return hd;
	}
	
	public int updateEmployeeVendorClaimsSubmitStatus(Map<String, String> headers, BigInteger empVendorClaimId) {
		int hd;
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		try {
			LOGGER.info("updateEmployeeVendorClaimsSubmitStatus for EmployeeVendorClaim : " + empVendorClaimId);
			hd = employeeVendorClaimsHdRepository.updateEmployeeVendorClaimsSubmitStatus(1, userCode, DateUtilities.getCurrentDate(), 
					empVendorClaimId);

		} catch (Exception e) {
			throw e;
		}
		return hd;
	}

	/*
	

	

	

	public Optional<PMSContractSupplierPriceHd> getMplByNo(Map<String, String> headers, String supplierNo) {
		Optional<PMSContractSupplierPriceHd> hd = null;
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		try {
			LOGGER.info("getMpl for Master Price List : " + supplierNo);

			if (roleId == 1) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractByNo(supplierNo);
			} else if (roleId == 2) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractNoAndVendorId(supplierNo, ownerId);
			} else if (roleId == 3) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractNoAndVendorId(supplierNo, ownerId);
			}

		} catch (Exception e) {
			throw e;
		}
		return hd;
	}

	@Transactional
	public List<SupplierVendorPojo> getContractSupplierVendor(BigInteger categoryId) {
		String query = "select PricesHd.VendorId, Vendor.VendorCode, Vendor.VendorName from PMSContractSupplierPriceHd PricesHd, AdmVendor Vendor where PricesHd.VendorId = Vendor.VendorId"
				+ " and PricesHd.ContractCategoryId = :categoryId "
				+ "and PricesHd.ValidFrom <= getdate() and PricesHd.ExpiryDate >= getdate()";

		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("categoryId", "" + categoryId);

		List<SupplierVendorPojo> searchResults = null;
		try {
			searchResults = jdbcTemplate.query(query, queryParams,
					new BeanPropertyRowMapper<>(SupplierVendorPojo.class));

		} catch (Exception e) {
			throw e;
		}

		return searchResults;
	}

	@Transactional
	public List<MplVendorPojo> getMasterPriceListVendors(Map<String, String> headers, Map<String, String> map) {

		Date dateRange = Date.valueOf(map.get("trxDate"));
		String query = "select hd.VendorId, hd.ContactPersonId, hd.SupplierContractId, hd.SupplierContractNo, vdr.vendorName, vdr.vendorCode \r\n"
				+ "from PMSContractSupplierPriceHd hd, admVendor vdr where hd.BranchId = vdr.BranchId \r\n"
				+ "and hd.VendorId = vdr.VendorId\r\n" + "and vdr.BranchId = :branchId\r\n"
				+ "and hd.IsCancel = 0 and  hd.ContractCategoryId = :contractCategoryId and hd.BargeId in (0, :bargeId) \r\n"
				+ "and hd.ValidFrom <= :date and hd.ExpiryDate >= :date;";

		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("date", "" + dateRange);
		queryParams.put("contractCategoryId", "" + map.get("contractCategoryId"));
		queryParams.put("bargeId", "" + map.get("bargeId"));
		queryParams.put("branchId", "" + String.valueOf(headers.get(CommonConstants.BRANCH_ID)));

		List<MplVendorPojo> searchResults = null;
		try {
			searchResults = jdbcTemplate.query(query, queryParams, new BeanPropertyRowMapper<>(MplVendorPojo.class));

		} catch (Exception e) {
			throw e;
		}

		return searchResults;
	}

*/
}
