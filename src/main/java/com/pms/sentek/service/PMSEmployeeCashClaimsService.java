package com.pms.sentek.service;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pms.common.apicall.ApiCallService;
import com.pms.common.constants.CommonConstants;
import com.pms.common.controller.BaseController;
import com.pms.common.entity.PMSEmployeeCashClaimsDt;
import com.pms.common.entity.PMSEmployeeCashClaimsHd;
import com.pms.common.entity.PMSEmployeeVendorClaimsHd;
import com.pms.common.exception.ResponseException;
import com.pms.common.pojo.BTSApprovalProcessPojo;
import com.pms.common.pojo.MplVendorPojo;
import com.pms.common.pojo.PMSEmployeeCashClaimsDtPojo;
import com.pms.common.pojo.PMSEmployeeCashClaimsHdPojo;
import com.pms.common.pojo.SupplierVendorPojo;
import com.pms.common.response.CustomResponse;
import com.pms.common.service.BaseService;
import com.pms.common.service.transaction.BaseServiceTrans;
import com.pms.common.utilities.DateUtilities;
import com.pms.common.utilities.Utilities;
import com.pms.sentek.repository.PMSEmployeeCashClaimsHdRepository;
import com.pms.sentek.service.transaction.PMSEmployeeCashClaimsServiceTransaction;

@Service
public class PMSEmployeeCashClaimsService extends BaseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSEmployeeCashClaimsService.class);
	@Autowired
	BaseServiceTrans baseSerTrans;

	@Autowired
	BaseService baseSer;
	@Autowired
	PMSEmployeeCashClaimsHdRepository employeeCashClaimsHdRepository;
	
	@Autowired
	PMSEmployeeCashClaimsServiceTransaction eccTransaction;
	
	PMSEmployeeCashClaimsHdPojo employeeCashClaimsHdPojo=new PMSEmployeeCashClaimsHdPojo();
	
	PMSEmployeeCashClaimsDtPojo employeeCashClaimsDtPojo=new PMSEmployeeCashClaimsDtPojo();
	
	List<SupplierVendorPojo> supplierVendorPojo=new ArrayList<SupplierVendorPojo>();
	
	List<MplVendorPojo> mplVendorPojoList=new ArrayList<MplVendorPojo>();


	ByteArrayOutputStream outputStream = null;
	@Value("${isms.email.url.suffix}")
	private String suffixKey;
	
	@Value("${ipms.hostName}")
	private String ipmsHostName;

	@Value("${ipms.port}")
	private String ipmsPort;
	@Value("${ipms.approvalProcess.module}")
	private String approvalModulePath;
	
	@Value("${ipms.approvalProcess.context}")
	private String ipmsapprovalProcessContext;

	public ResponseEntity<?> getEmployeeCashClaimsByDateRange(Map<String, String> headers, Map<String, String> map)
			throws Exception {
		List<PMSEmployeeCashClaimsHdPojo> employeeCashClaimsHdList = null;
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		Date startDate = Date.valueOf(map.get("startDate"));
		Date endDate = Date.valueOf(map.get("endDate"));
		try {

			if ( roleId == CommonConstants.ROLE_ID_BACK_OFFICE ) {
				employeeCashClaimsHdList = eccTransaction.getEmployeeCashClaimsByDateRange( headers, map );
			} else if (roleId == CommonConstants.ROLE_ID_VENDOR) {
//				quotattionHdList = quotationSeTrans.getQuotationsByDateRangeForVendor(startDate,
////						Utilities.addDays(endDate, 1), ownerId, moduleId);
			}
			
			if ( employeeCashClaimsHdList == null ) {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null, new ResponseException(HttpStatus.NOT_FOUND, "No records found in this search criteria."));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			} else if ( employeeCashClaimsHdList.size() == 0 ) {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null, new ResponseException(HttpStatus.NOT_FOUND, "No records found in this search criteria."));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.error("We have received the error " + ex.getMessage());
			baseSerTrans.saveErrorLog(headers, ex, "PMSEmployeeCashClaimsService");
			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, employeeCashClaimsHdList, null);
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}
	
	public ResponseEntity<?> getEmployeeCashClaimsById(Map<String, String> headers, BigInteger employeeClaimId) {
		employeeCashClaimsHdPojo = null;
		Optional<PMSEmployeeCashClaimsHd> hd = null;

		try {
			hd = eccTransaction.getEmployeeCashClaimsById(headers, employeeClaimId);
			if (hd != null && hd.isPresent()) {
				employeeCashClaimsHdPojo = conertDataFromEntToPojo(hd.get());
			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for this Employee Claim ID"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "EmployeeCashClaims");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null, new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, employeeCashClaimsHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	private PMSEmployeeCashClaimsHdPojo conertDataFromEntToPojo(PMSEmployeeCashClaimsHd hd) {
		System.out.println("hd.getEmployeeCashClaimsDetails().size().."+hd.getEmployeeCashClaimsDetails().size());
		employeeCashClaimsHdPojo = new PMSEmployeeCashClaimsHdPojo();
		employeeCashClaimsDtPojo = new PMSEmployeeCashClaimsDtPojo();

		BeanUtils.copyProperties(hd, employeeCashClaimsHdPojo);
		employeeCashClaimsHdPojo.setEmployeeClaimId(hd.getPk().getEmployeeClaimId());
		employeeCashClaimsHdPojo.setEmployeeClaimNo(hd.getPk().getEmployeeClaimNo());
		//employeeCashClaimsHdPojo.setVendorId(hd.getAdmVendorId().getVendorId());
		
		employeeCashClaimsHdPojo.setEmployeeCode(hd.getEmployeeId().getEmployeeCode() );
		employeeCashClaimsHdPojo.setEmployeeName(hd.getEmployeeId().getEmployeeName() );
		employeeCashClaimsHdPojo.setPmsStatusCode(hd.getAdmPMSStatus().getPmsStatusCode() );
		employeeCashClaimsHdPojo.setPmsStatusName(hd.getAdmPMSStatus().getPmsStatusName() );
		employeeCashClaimsHdPojo.setEmployeeId(hd.getEmployeeId().getEmployeeId() );
		employeeCashClaimsHdPojo.setStatusId(hd.getAdmPMSStatus().getPmsStatusId() );
	
		if (hd.getEmployeeCashClaimsDetails() != null && hd.getEmployeeCashClaimsDetails().size() > 0) {

			for (PMSEmployeeCashClaimsDt details : hd.getEmployeeCashClaimsDetails()) {
				PMSEmployeeCashClaimsDtPojo evcDtPojo = new PMSEmployeeCashClaimsDtPojo();
				BeanUtils.copyProperties(details, evcDtPojo);
				evcDtPojo.setEmployeeClaimId(details.getPk().getEmployeeClaimId());
				evcDtPojo.setEmployeeClaimNo(details.getPk().getEmployeeClaimNo());
				evcDtPojo.setItemNo(details.getPk().getItemNo());
				evcDtPojo.setBargeCode(details.getBargeId().getBargeCode());
				evcDtPojo.setBargeName(details.getBargeId().getBargeName());
				evcDtPojo.setiMPACode(details.getiMPACodeId().getiMPACode());
				evcDtPojo.setMainCode(details.getMainCodeId().getMainCode());
				evcDtPojo.setMainCodeDescription(details.getMainCodeId().getMainCodeDescription());
				evcDtPojo.setUomCode(details.getuOMId().getUomCode());
				evcDtPojo.setUomName(details.getuOMId().getUomName());
				evcDtPojo.setBargeId(details.getBargeId().getBargeId());
				evcDtPojo.setiMPACodeId(details.getiMPACodeId().getiMPACodeId());
				evcDtPojo.setiMPACategoryDescription(details.getiMPACodeId().getiMPACategoryDescription());
				evcDtPojo.setMainCodeId(details.getMainCodeId().getMainCodeId());
				evcDtPojo.setuOMId(details.getuOMId().getUomId());
				
				evcDtPojo.setgSTId(details.getgSTId().getGstId());
				evcDtPojo.setgSTName(details.getgSTId().getGstName());
				evcDtPojo.setgSTCode(details.getgSTId().getGstCode());
				
				employeeCashClaimsHdPojo.addEmployeeCashClaimsDetails(evcDtPojo);
			}
		}
		return employeeCashClaimsHdPojo;
	}
	
	public ResponseEntity<?> saveEmployeeCashClaims(Map<String, String> headers, PMSEmployeeCashClaimsHdPojo req) {
		employeeCashClaimsHdPojo=new PMSEmployeeCashClaimsHdPojo();
		try {
			employeeCashClaimsHdPojo = eccTransaction.saveEmployeeCashClaims(headers, req);
			
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "saveEmployeeCashClaims");
			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, employeeCashClaimsHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	
	public ResponseEntity<?> editEmployeeCashClaims(Map<String, String> headers, PMSEmployeeCashClaimsHdPojo req) {
		employeeCashClaimsHdPojo=new PMSEmployeeCashClaimsHdPojo();
		try {
			employeeCashClaimsHdPojo = eccTransaction.editEmployeeCashClaims(headers, req);
			
		} catch (EntityNotFoundException entNotFound) {
			LOGGER.error("We have received the error " + entNotFound.getMessage());
			baseSerTrans.saveErrorLog(headers, entNotFound, "EmployeeCashClaims");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null, new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, entNotFound.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);

		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "EmployeeCashClaims");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null, new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);

		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, employeeCashClaimsHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	public ResponseEntity<?> updateEmployeeCashClaimsSubmitStatus(Map<String, String> headers, Map<String, String> bodyHeader) {
		int hdPojo;
		Optional<PMSEmployeeCashClaimsHd> hd = null;
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));

		BigInteger employeeClaimId = new BigInteger(String.valueOf(bodyHeader.get("employeeClaimId")));
		String employeeClaimNo = String.valueOf(bodyHeader.get("employeeClaimNo"));
		//BigInteger vendorId = new BigInteger(String.valueOf(bodyHeader.get("vendorId")));
		
		BigInteger vendorId = BigInteger.ZERO;
		Map<String, String> bodyContent = new HashMap<>();
		


		try {
			hd = eccTransaction.getEmployeeCashClaimsById(headers,employeeClaimId);
			if (hd.isPresent()) {
				hdPojo = eccTransaction.updateEmployeeCashClaimsSubmitStatus(headers, employeeClaimId);
				
				bodyContent.put("DOCUMENT_ID", "" + employeeClaimId);
				bodyContent.put("DOCUMENT_NO", employeeClaimNo);
				bodyContent.put("FORWARDBY_USERGROUPID", "2");
				bodyContent.put("FORWARDTO_USERGROUPID", "2");
				bodyContent.put("FORWARDTOEMPLOYEEID", "0");
				bodyContent.put("REMARKS", "");
				bodyContent.put("BARGE_ID", "" + BigInteger.ZERO);
				
				createNewApprovalProcessRecord(headers, bodyContent);

				baseSer.letsDoEmailJob(branchId, transactionId, moduleId, userCode, employeeClaimId, employeeClaimNo,
						DateUtilities.getCurrentDate(), vendorId, suffixKey.trim(), outputStream);
				
				 baseSerTrans.updateProcessStatusDescInHD( branchId,  moduleId,  transactionId, employeeClaimId,  CommonConstants.EMPLOYEE_CASH_CLAIM_TABLE_NAME,  "submitted");

			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested Employee Vendor ID"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			String errorMessage  = baseSerTrans.saveErrorLog(headers, e, "PMSEmployeeCashClaimsService");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage ));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, "Successfully Submitted", null);
		return ResponseEntity.ok().body(resp);
	}
	
	
	private void createNewApprovalProcessRecord(Map<String, String> headers, Map<String, String> bodyContent ) throws JsonProcessingException {
		
		/*
		 * Map<String, String> bodyContent = new HashMap<>();
		 * bodyContent.put("DOCUMENT_ID", "" + employeeClaimId);
		 * bodyContent.put("DOCUMENT_NO", employeeClaimNo);
		 * bodyContent.put("FORWARDBY_USERGROUPID", "2");
		 * bodyContent.put("FORWARDTO_USERGROUPID", "2");
		 * bodyContent.put("FORWARDTOEMPLOYEEID", "0"); bodyContent.put("REMARKS", "");
		 * bodyContent.put("BARGE_ID", "" + BigInteger.ZERO);
		 */
		
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		
		BigInteger bargeId = new BigInteger(String.valueOf(bodyContent.get("BARGE_ID")));
		BigInteger documentId = new BigInteger(String.valueOf(bodyContent.get("DOCUMENT_ID")));
		String documentNo = String.valueOf(bodyContent.get("DOCUMENT_NO"));
		String remarks = String.valueOf(bodyContent.get("REMARKS"));
		BigInteger forwardByUserGroupId = new BigInteger(String.valueOf(bodyContent.get("FORWARDBY_USERGROUPID")));
		BigInteger forwardToUserGroupId = new BigInteger(String.valueOf(bodyContent.get("FORWARDTO_USERGROUPID")));
		BigInteger forwardToEmployeeId = new BigInteger(String.valueOf(bodyContent.get("FORWARDTOEMPLOYEEID")));
		
		String insertRecordOperation = "createNewVersionedRecord";
		Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());
		final String approvalServiceURL = ipmsHostName + ":" + ipmsPort + ipmsapprovalProcessContext + approvalModulePath + "/"
				+ insertRecordOperation;
		
		ObjectMapper mapper = new ObjectMapper();
		BTSApprovalProcessPojo approvalProcessDetails = new BTSApprovalProcessPojo();
		LOGGER.info("approvalServiceURL.."+approvalServiceURL);
		approvalProcessDetails.setApprovalStatusId(1);
		approvalProcessDetails.setApproveBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
		approvalProcessDetails.setApproveDate(DateUtilities.getCurrentDate());
		approvalProcessDetails.setBargeId(bargeId);
		approvalProcessDetails.setBrageName("");
		approvalProcessDetails.setBranchId(branchId);
		approvalProcessDetails.setCreateBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
		approvalProcessDetails.setCreateDate(DateUtilities.getCurrentDate());
		approvalProcessDetails.setDocumentId(documentId);
		approvalProcessDetails.setDocumentNo(documentNo);
		approvalProcessDetails.setModuleId(moduleId);
		approvalProcessDetails.setTransactionId(transactionId);
		approvalProcessDetails.setForwardBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));

		approvalProcessDetails.setForwardBy_UserGroupId(forwardByUserGroupId);
		approvalProcessDetails.setForwardDate(DateUtilities.getCurrentDate());

		approvalProcessDetails.setForwardTo_UserGroupId(forwardToUserGroupId);
		approvalProcessDetails.setForwardToEmployeeId(forwardToEmployeeId);
		approvalProcessDetails.setRemarks(remarks);
		
		approvalProcessDetails.setTrxDate(DateUtilities.getCurrentDate());
		List<BTSApprovalProcessPojo> jsonList = new ArrayList<BTSApprovalProcessPojo>();
		jsonList.add(approvalProcessDetails);
		
		String json = mapper.writeValueAsString(jsonList);
		
		 LOGGER.info("request String Approval process from Employee Vendor Claims:: "+json);
		CustomResponse response = ApiCallService.callService(approvalServiceURL, HttpMethod.POST, json,createHttpHeadersForRequistion(headers));
		LOGGER.info("Placed a Employee Vendor Claims rec for approval process - status " +response.getStatus().value());
		
	}
	
	/*
	
	public ResponseEntity<?> getMplByNo(Map<String, String> headers, String supplierNo) {
		contractSupplierPriceHdPojo = null;
		Optional<PMSContractSupplierPriceHd> hd = null;

		try {
			hd = mplTransaction.getMplByNo(headers, supplierNo);
			if (hd.isPresent()) {
				contractSupplierPriceHdPojo = conertDataFromEntToPojo(hd.get());
			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, contractSupplierPriceHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	
	@Transactional
	public ResponseEntity<?> changeRequisitionStatus(Map<String, String> headers, BigInteger supplierContractId
			) {
		boolean transactionSuccess = false;

		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		String trxNo = "";
		String referenceNo = "";
		BigInteger bargeid = BigInteger.ZERO;
		
		try {
			LOGGER.info("request to deleteMpl for Master Price List : " + supplierContractId);
			Optional<PMSContractSupplierPriceHd> hdRec = contractSupplierPriceHdRepository.findMplSupplierContractById(supplierContractId);
			if (hdRec.isPresent()) {
				
					hdRec.get().setIsCancel(1);
					hdRec.get().setCancelBy(userCode);
					hdRec.get().setCancelDate(DateUtilities.getCurrentDate());
				

					bargeid = hdRec.get().getAdmBarge().getBargeId();
					contractSupplierPriceHdRepository.saveAndFlush(hdRec.get());
					transactionSuccess = true;
		
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.MASTER_PRICE_LIST_TABLE_NAME, trxNo, referenceNo, supplierContractId + "", "Cancel",
						bargeid + "");
			}
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK,
				"Successfully Changed The Status for Master Price List  :: " + supplierContractId, null);
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}
	
	public ResponseEntity<?> getContractSupplierVendor(Map<String, String> headers, BigInteger reqId) {
		supplierVendorPojo = new ArrayList<SupplierVendorPojo>();
		
		try {
			supplierVendorPojo = mplTransaction.getContractSupplierVendor(reqId);
			if (supplierVendorPojo.size()==0) {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, supplierVendorPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	
	public ResponseEntity<?> getMasterPriceListVendors(Map<String, String> headers,  Map<String, String> map) {
		mplVendorPojoList = new ArrayList<MplVendorPojo>();
		
		try {
			mplVendorPojoList = mplTransaction.getMasterPriceListVendors(headers,map);
			if (mplVendorPojoList.size()==0) {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, mplVendorPojoList, null);
		return ResponseEntity.ok().body(resp);
	}

*/	
}
