package com.pms.sentek.service;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pms.common.apicall.ApiCallService;
import com.pms.common.constants.CommonConstants;
import com.pms.common.controller.BaseController;
import com.pms.common.entity.PMSEmployeeVendorClaimsDt;
import com.pms.common.entity.PMSEmployeeVendorClaimsHd;
import com.pms.common.exception.ResponseException;
import com.pms.common.pojo.BTSApprovalProcessPojo;
import com.pms.common.pojo.MplVendorPojo;
import com.pms.common.pojo.PMSEmployeeVendorClaimsDtPojo;
import com.pms.common.pojo.PMSEmployeeVendorClaimsHdPojo;
import com.pms.common.pojo.SupplierVendorPojo;
import com.pms.common.response.CustomResponse;
import com.pms.common.service.BaseService;
import com.pms.common.service.transaction.BaseServiceTrans;
import com.pms.common.utilities.DateUtilities;
import com.pms.common.utilities.Utilities;
import com.pms.sentek.repository.PMSEmployeeVendorClaimsHdRepository;
import com.pms.sentek.service.transaction.PMSEmployeeVendorClaimsServiceTransaction;

@Service
public class PMSEmployeeVendorClaimsService extends BaseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSEmployeeVendorClaimsService.class);
	@Autowired
	BaseServiceTrans baseSerTrans;

	@Autowired
	BaseService baseSer;
	@Autowired
	PMSEmployeeVendorClaimsHdRepository employeeVendorClaimsHdRepository;
	
	@Autowired
	PMSEmployeeVendorClaimsServiceTransaction evcTransaction;
	
	PMSEmployeeVendorClaimsHdPojo employeeVendorClaimsHdPojo=new PMSEmployeeVendorClaimsHdPojo();
	
	PMSEmployeeVendorClaimsDtPojo employeeVendorClaimsDtPojo=new PMSEmployeeVendorClaimsDtPojo();
	
	List<SupplierVendorPojo> supplierVendorPojo=new ArrayList<SupplierVendorPojo>();
	
	List<MplVendorPojo> mplVendorPojoList=new ArrayList<MplVendorPojo>();
	
	ByteArrayOutputStream outputStream = null;
	@Value("${isms.email.url.suffix}")
	private String suffixKey;
	
	@Value("${ipms.hostName}")
	private String ipmsHostName;

	@Value("${ipms.port}")
	private String ipmsPort;
	@Value("${ipms.approvalProcess.module}")
	private String approvalModulePath;
	
	@Value("${ipms.approvalProcess.context}")
	private String ipmsapprovalProcessContext;
	
	public ResponseEntity<?> getEmployeeVendorClaimsByDateRange(Map<String, String> headers, Map<String, String> map)
			throws Exception {
		List<PMSEmployeeVendorClaimsHdPojo> employeeVendorClaimsHdList = null;
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		Date startDate = Date.valueOf(map.get("startDate"));
		Date endDate = Date.valueOf(map.get("endDate"));
		try {

			if (roleId == CommonConstants.ROLE_ID_BACK_OFFICE) {
				employeeVendorClaimsHdList = evcTransaction.getEmployeeVendorClaimsByDateRange(headers, map);
			} else if (roleId == CommonConstants.ROLE_ID_VENDOR) {
//				quotattionHdList = quotationSeTrans.getQuotationsByDateRangeForVendor(startDate,
////						Utilities.addDays(endDate, 1), ownerId, moduleId);
			}
			
			if ( employeeVendorClaimsHdList == null ) {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null, new ResponseException(HttpStatus.NOT_FOUND, "No records found in this search criteria."));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			} else if ( employeeVendorClaimsHdList.size() == 0 ) {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null, new ResponseException(HttpStatus.NOT_FOUND, "No records found in this search criteria."));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}

		} catch (Exception ex) {

			LOGGER.error("We have received the error " + ex.getMessage());
			baseSerTrans.saveErrorLog(headers, ex, "PMSEmployeeCashClaimsService");
			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, employeeVendorClaimsHdList, null);
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}
	
	public ResponseEntity<?> getEmployeeVendorClaimsById(Map<String, String> headers, BigInteger empVendorClaimId) {
		employeeVendorClaimsHdPojo = null;
		Optional<PMSEmployeeVendorClaimsHd> hd = null;

		try {
			hd = evcTransaction.getEmployeeVendorClaimsById(headers, empVendorClaimId);
			if (hd.isPresent()) {
				employeeVendorClaimsHdPojo = conertDataFromEntToPojo(hd.get());
			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "EmployeeVendorClaims");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, employeeVendorClaimsHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	private PMSEmployeeVendorClaimsHdPojo conertDataFromEntToPojo(PMSEmployeeVendorClaimsHd hd) {
		System.out.println("hd.getEmployeeVendorClaimsDetails().size().."+hd.getEmployeeVendorClaimsDetails().size());
		employeeVendorClaimsHdPojo = new PMSEmployeeVendorClaimsHdPojo();
		employeeVendorClaimsDtPojo = new PMSEmployeeVendorClaimsDtPojo();

		BeanUtils.copyProperties(hd, employeeVendorClaimsHdPojo);
		employeeVendorClaimsHdPojo.setEmpVendorClaimId(hd.getPk().getEmpVendorClaimId());
		employeeVendorClaimsHdPojo.setEmpVendorClaimNo(hd.getPk().getEmpVendorClaimNo());
		employeeVendorClaimsHdPojo.setVendorId(hd.getVendorId().getVendorId());
		employeeVendorClaimsHdPojo.setVendorCode(hd.getVendorId().getVendorCode());
		employeeVendorClaimsHdPojo.setVendorName(hd.getVendorId().getVendorName());
		employeeVendorClaimsHdPojo.setContactPersonId(hd.getContactPersonId().getAddressId() );
		employeeVendorClaimsHdPojo.setContact(hd.getContactPersonId().getContact() );
		employeeVendorClaimsHdPojo.setEmployeeId(hd.getEmployeeId().getEmployeeId() );
		employeeVendorClaimsHdPojo.setEmployeeCode(hd.getEmployeeId().getEmployeeCode() );
		employeeVendorClaimsHdPojo.setEmployeeName(hd.getEmployeeId().getEmployeeName() );
		employeeVendorClaimsHdPojo.setCurrencyId(hd.getCurrencyId().getCurrencyId() );
		employeeVendorClaimsHdPojo.setCurrencyCode(hd.getCurrencyId().getCurrencyCode() );
		employeeVendorClaimsHdPojo.setCurrencyName(hd.getCurrencyId().getCurrencyName() );
		employeeVendorClaimsHdPojo.setStatusId(hd.getAdmPMSStatus().getPmsStatusId() );
		employeeVendorClaimsHdPojo.setPmsStatusCode(hd.getAdmPMSStatus().getPmsStatusCode() );
		employeeVendorClaimsHdPojo.setPmsStatusName(hd.getAdmPMSStatus().getPmsStatusName() );
	
		if (hd.getEmployeeVendorClaimsDetails() != null && hd.getEmployeeVendorClaimsDetails().size() > 0) {

			for (PMSEmployeeVendorClaimsDt details : hd.getEmployeeVendorClaimsDetails()) {
				PMSEmployeeVendorClaimsDtPojo evcDtPojo = new PMSEmployeeVendorClaimsDtPojo();
				BeanUtils.copyProperties(details, evcDtPojo);
				evcDtPojo.setEmpVendorClaimId(details.getPk().getEmpVendorClaimId());
				evcDtPojo.setEmpVendorClaimNo(details.getPk().getEmpVendorClaimNo());
				evcDtPojo.setItemNo(details.getPk().getItemNo());
				evcDtPojo.setBargeId(details.getBargeId().getBargeId());
				evcDtPojo.setBargeCode(details.getBargeId().getBargeCode());
				evcDtPojo.setBargeName(details.getBargeId().getBargeName());
				evcDtPojo.setVesselDepartmentId(details.getVesselDepartmentId().getVesselDepartmentId());
				evcDtPojo.setVesselDepartmentCode(details.getVesselDepartmentId().getVesselDepartmentCode());
				evcDtPojo.setVesselDepartmentName(details.getVesselDepartmentId().getVesselDepartmentName());
				evcDtPojo.setiMPACodeId(details.getiMPACodeId().getiMPACodeId());
				evcDtPojo.setiMPACode(details.getiMPACodeId().getiMPACode());
				evcDtPojo.setiMPACategoryDescription(details.getiMPACodeId().getiMPACategoryDescription());
				evcDtPojo.setMainCodeId(details.getMainCodeId().getMainCodeId());
				evcDtPojo.setMainCode(details.getMainCodeId().getMainCode());
				evcDtPojo.setMainCodeDescription(details.getMainCodeId().getMainCodeDescription());
				evcDtPojo.setuOMId(details.getuOMId().getUomId());
				evcDtPojo.setUomCode(details.getuOMId().getUomCode());
				evcDtPojo.setUomName(details.getuOMId().getUomName());
				
				evcDtPojo.setgSTId(details.getgSTId().getGstId());
				evcDtPojo.setgSTName(details.getgSTId().getGstName());
				evcDtPojo.setgSTCode(details.getgSTId().getGstCode());
				
				employeeVendorClaimsHdPojo.addEmployeeVendorClaimsDetails(evcDtPojo);
			}
		}
		return employeeVendorClaimsHdPojo;
	}
	
	public ResponseEntity<?> saveEmployeeVendorClaims(Map<String, String> headers, PMSEmployeeVendorClaimsHdPojo req) {
		employeeVendorClaimsHdPojo=new PMSEmployeeVendorClaimsHdPojo();
		try {
			employeeVendorClaimsHdPojo = evcTransaction.saveEmployeeVendorClaims(headers, req);
			
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "saveEmployeeVendorClaims");
			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, employeeVendorClaimsHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	
	public ResponseEntity<?> editEmployeeVendorClaims(Map<String, String> headers, PMSEmployeeVendorClaimsHdPojo req) {
		employeeVendorClaimsHdPojo=new PMSEmployeeVendorClaimsHdPojo();
		try {
			employeeVendorClaimsHdPojo = evcTransaction.editEmployeeVendorClaims(headers, req);
			
		} catch (EntityNotFoundException entNotFound) {
			LOGGER.error("We have received the error " + entNotFound.getMessage());
			baseSerTrans.saveErrorLog(headers, entNotFound, "EmployeeVendorClaims");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null, new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, entNotFound.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);

		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "EmployeeVendorClaims");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null, new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);

		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, employeeVendorClaimsHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	@Transactional
	public ResponseEntity<?> changeRequisitionStatus(Map<String, String> headers, BigInteger empVendorClaimId
			) {
		boolean transactionSuccess = false;

		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		String trxNo = "";
		String referenceNo = "";
		BigInteger bargeid = BigInteger.ZERO;
		
		try {
			LOGGER.info("request to deleteMpl for Master Price List : " + empVendorClaimId);
			Optional<PMSEmployeeVendorClaimsHd> hdRec = employeeVendorClaimsHdRepository.fineEmployeeVendorClaimsById(empVendorClaimId);
			if (hdRec.isPresent()) {
				
					hdRec.get().setIsCancel(1);
					hdRec.get().setCancelBy(userCode);
					hdRec.get().setCancelDate(DateUtilities.getCurrentDate());

					employeeVendorClaimsHdRepository.saveAndFlush(hdRec.get());
					transactionSuccess = true;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.MASTER_PRICE_LIST_TABLE_NAME, trxNo, referenceNo, empVendorClaimId + "", "Cancel",
						bargeid + "");
			}
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK,
				"Successfully Changed The Status for Employee Vendor Claims:: " + empVendorClaimId, null);
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}
	
	public ResponseEntity<?> updateEmployeeVendorClaimsPostingInfo(Map<String, String> headers, Map<String, String> bodyHeader) {
		int hdPojo;
		Optional<PMSEmployeeVendorClaimsHd> hd = null;
		BigInteger empVendorClaimId = new BigInteger(String.valueOf(bodyHeader.get("empVendorClaimId")));
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		//Overriding the TransactionStatus with 6
		headers.put(CommonConstants.TRANSACTION_ID, ""+6);
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));

		try {
			hd = evcTransaction.getEmployeeVendorClaimsById(headers, empVendorClaimId);
			if (hd.isPresent()) {
				hdPojo = evcTransaction.updateEmployeeVendorClaimsPostingInfo(headers, empVendorClaimId);
				//baseSer.letsDoEmailJob(branchId, transactionId, moduleId, userCode, empVendorClaimId, null, DateUtilities.getCurrentDate(), BigInteger.ZERO, suffixKey.trim(), null ,0,0);
			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			String errorMessage  = baseSerTrans.saveErrorLog(headers, e, "PMSEmployeeVendorClaimsService");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, "Posting Information Updted Successfully (Employee Vendor Claims)", null);
		return ResponseEntity.ok().body(resp);
	}
	
	public ResponseEntity<?> updateEmployeeVendorClaimsSubmitStatus(Map<String, String> headers, Map<String, String> bodyHeader) {
		int hdPojo;
		Optional<PMSEmployeeVendorClaimsHd> hd = null;
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));

		BigInteger empVendorClaimId = new BigInteger(String.valueOf(bodyHeader.get("empVendorClaimId")));
		String empVendorClaimNo = String.valueOf(bodyHeader.get("empVendorClaimNo"));
		BigInteger vendorId = new BigInteger(String.valueOf(bodyHeader.get("vendorId")));

		try {
			hd = evcTransaction.getEmployeeVendorClaimsById(headers,empVendorClaimId);
			if (hd.isPresent()) {
				hdPojo = evcTransaction.updateEmployeeVendorClaimsSubmitStatus(headers, empVendorClaimId);
				
				placeEVCRecForApproval(headers,DateUtilities.getCurrentDate(), BigInteger.ZERO,branchId,empVendorClaimId,empVendorClaimNo);

				baseSer.letsDoEmailJob(branchId, transactionId, moduleId, userCode, empVendorClaimId, empVendorClaimNo,
						DateUtilities.getCurrentDate(), vendorId, suffixKey.trim(), outputStream);
				
				 baseSerTrans.updateProcessStatusDescInHD( branchId,  moduleId,  transactionId, empVendorClaimId,  CommonConstants.EMPLOYEE_VENDOR_CLAIM_TABLE_NAME,  "submitted");

			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested Employee Vendor ID"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			String errorMessage  = baseSerTrans.saveErrorLog(headers, e, "PMSEmployeeVendorClaimsService");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage ));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, "Successfully Submitted", null);
		return ResponseEntity.ok().body(resp);
	}
	
	private void placeEVCRecForApproval(Map<String, String> headers,Timestamp createdDate, BigInteger bargeId,BigInteger branchId,BigInteger empVendorClaimId,String empVendorClaimNo ) throws JsonProcessingException {
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		String insertRecordOperation = "createNewVersionedRecord";
		Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());
		final String approvalServiceURL = ipmsHostName + ":" + ipmsPort + ipmsapprovalProcessContext + approvalModulePath + "/"
				+ insertRecordOperation;
		
		ObjectMapper mapper = new ObjectMapper();
		BTSApprovalProcessPojo approvalProcessDetails = new BTSApprovalProcessPojo();
		LOGGER.info("approvalServiceURL.."+approvalServiceURL);
		approvalProcessDetails.setApprovalStatusId(1);
		approvalProcessDetails.setApproveBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
		approvalProcessDetails.setApproveDate(createdDate);
		approvalProcessDetails.setBargeId(bargeId);
		approvalProcessDetails.setBrageName("");
		approvalProcessDetails.setBranchId(branchId);
		approvalProcessDetails.setCreateBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
		approvalProcessDetails.setCreateDate(DateUtilities.getCurrentDate());
		approvalProcessDetails.setDocumentId(empVendorClaimId);
		approvalProcessDetails.setDocumentNo(empVendorClaimNo);
		approvalProcessDetails.setModuleId(moduleId);
		approvalProcessDetails.setTransactionId(transactionId);
		approvalProcessDetails.setForwardBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
		//TODO 
		approvalProcessDetails.setForwardBy_UserGroupId(new BigInteger("2"));
		approvalProcessDetails.setForwardDate(DateUtilities.getCurrentDate());
		//TODO
		approvalProcessDetails.setForwardTo_UserGroupId(new BigInteger("2"));
		approvalProcessDetails.setForwardToEmployeeId(new BigInteger("0"));
		approvalProcessDetails.setRemarks("");
		
		approvalProcessDetails.setTrxDate(DateUtilities.getCurrentDate());
		List<BTSApprovalProcessPojo> jsonList = new ArrayList<BTSApprovalProcessPojo>();
		jsonList.add(approvalProcessDetails);
		
		String json = mapper.writeValueAsString(jsonList);
		
		 LOGGER.info("request String Approval process from Employee Vendor Claims:: "+json);
		CustomResponse response = ApiCallService.callService(approvalServiceURL, HttpMethod.POST, json,createHttpHeadersForRequistion(headers));
		LOGGER.info("Placed a Employee Vendor Claims rec for approval process - status " +response.getStatus().value());
		
	}
	
	/*
	
	public ResponseEntity<?> getMplByNo(Map<String, String> headers, String supplierNo) {
		contractSupplierPriceHdPojo = null;
		Optional<PMSContractSupplierPriceHd> hd = null;

		try {
			hd = mplTransaction.getMplByNo(headers, supplierNo);
			if (hd.isPresent()) {
				contractSupplierPriceHdPojo = conertDataFromEntToPojo(hd.get());
			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, contractSupplierPriceHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	
	
	
	public ResponseEntity<?> getContractSupplierVendor(Map<String, String> headers, BigInteger reqId) {
		supplierVendorPojo = new ArrayList<SupplierVendorPojo>();
		
		try {
			supplierVendorPojo = mplTransaction.getContractSupplierVendor(reqId);
			if (supplierVendorPojo.size()==0) {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, supplierVendorPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	
	public ResponseEntity<?> getMasterPriceListVendors(Map<String, String> headers,  Map<String, String> map) {
		mplVendorPojoList = new ArrayList<MplVendorPojo>();
		
		try {
			mplVendorPojoList = mplTransaction.getMasterPriceListVendors(headers,map);
			if (mplVendorPojoList.size()==0) {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, mplVendorPojoList, null);
		return ResponseEntity.ok().body(resp);
	}

*/	
}
