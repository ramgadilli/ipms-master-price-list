package com.pms.mpl.controller;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pms.common.controller.BaseController;
import com.pms.common.exception.HeaderException;
import com.pms.common.pojo.PMSContractSupplierPriceHdPojo;
import com.pms.mpl.service.PMSMasterPriceListService;

@RestController
@RequestMapping("/masterPriceList")
public class PMSMasterPriceListController extends BaseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSMasterPriceListController.class);

	@Autowired
	PMSMasterPriceListService mplService;
	
	
	
	
	@PostMapping(value = "/getMasterPriceListByDateRange", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getQuotationListByDateRange(@RequestHeader Map<String, String> headers,@RequestBody Map<String,String> map) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			validateRequestBodyDateRange(map);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMasterPriceListByDateRange(headers, map);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceController.getMasterPriceListByDateRange "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));

		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}

	@PostMapping(value = "/saveMasterPriceList", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveRequisition(@RequestHeader Map<String, String> headers,
			@RequestBody PMSContractSupplierPriceHdPojo req) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = mplService.saveMpl(headers, req);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.saveMasterPriceList "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));

		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@PutMapping(value = "/editMasterPriceList/{supplierContractId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> editRequisition(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger supplierContractId, @RequestBody PMSContractSupplierPriceHdPojo req) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);

			if (!supplierContractId.equals(req.getSupplierContractId())) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body("Supplier Contract Id(In Master Price List Object) Is Not Matched With Path Value ");
			}

			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = mplService.editMpl(headers, req);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.editMasterPriceList "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
//
	@GetMapping(value = "/getMasterPriceList/{supplierId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getRequisition(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger supplierId) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMpl(headers, supplierId);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.getMasterPriceList "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}

	@GetMapping(value = "/getMasterPriceListByNo/{supplierNo}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getRequisitionByRequisitionNo(@RequestHeader Map<String, String> headers,
			@PathVariable String supplierNo) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMplByNo(headers, supplierNo);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.getMasterPriceListByNo "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}


	@PutMapping(value = "/changeMasterPriceListStatus/{supplierId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> changeRequisitionStatus(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger supplierId, @RequestBody Map<String, String> status) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);

			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = mplService.changeRequisitionStatus(headers, supplierId);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.changeMasterPriceListStatus "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}

	

	
	@GetMapping(value = "/getContractSupplierVendor/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getContractSupplierVendor(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger categoryId) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getContractSupplierVendor(headers, categoryId);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.getContractSupplierVendor "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	
	@PostMapping(value = "/getMasterPriceListVendors", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getContractSupplierVendor(@RequestHeader Map<String, String> headers
			,@RequestBody Map<String,String> map) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMasterPriceListVendors(headers, map);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterPriceList.getMasterPriceListVendors "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}

}
