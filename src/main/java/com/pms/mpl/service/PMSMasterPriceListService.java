package com.pms.mpl.service;

import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pms.common.constants.CommonConstants;
import com.pms.common.entity.PMSContractSupplierPriceDt;
import com.pms.common.entity.PMSContractSupplierPriceHd;
import com.pms.common.exception.ResponseException;
import com.pms.common.pojo.MplVendorPojo;
import com.pms.common.pojo.PMSContractSupplierPriceDtPojo;
import com.pms.common.pojo.PMSContractSupplierPriceHdPojo;
import com.pms.common.pojo.SupplierVendorPojo;
import com.pms.common.response.CustomResponse;
import com.pms.common.service.BaseService;
import com.pms.common.service.transaction.BaseServiceTrans;
import com.pms.common.utilities.DateUtilities;
import com.pms.common.utilities.Utilities;
import com.pms.mpl.repository.PMSContractSupplierPriceHdRepository;
import com.pms.mpl.service.transaction.PMSMasterPriceListServiceTransaction;

@Service
public class PMSMasterPriceListService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSMasterPriceListService.class);
	@Autowired
	BaseServiceTrans baseSerTrans;

	@Autowired
	BaseService baseSer;
	@Autowired
	PMSContractSupplierPriceHdRepository contractSupplierPriceHdRepository;
	
	@Autowired
	PMSMasterPriceListServiceTransaction mplTransaction;
	
	PMSContractSupplierPriceHdPojo contractSupplierPriceHdPojo=new PMSContractSupplierPriceHdPojo();
	
	PMSContractSupplierPriceDtPojo contractSupplierPriceDtPojo=new PMSContractSupplierPriceDtPojo();
	
	List<SupplierVendorPojo> supplierVendorPojo=new ArrayList<SupplierVendorPojo>();
	
	List<MplVendorPojo> mplVendorPojoList=new ArrayList<MplVendorPojo>();



	public ResponseEntity<?> getMasterPriceListByDateRange(Map<String, String> headers, Map<String, String> map)
			throws Exception {
		List<PMSContractSupplierPriceHdPojo> quotattionHdList = null;
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		Date startDate = Date.valueOf(map.get("startDate"));
		Date endDate = Date.valueOf(map.get("endDate"));
		try {

			if (roleId == CommonConstants.ROLE_ID_BACK_OFFICE ) {
				quotattionHdList = mplTransaction.getMasterPriceListByDateRangeForBackOffice(headers, map);
			} else if (roleId == CommonConstants.ROLE_ID_VENDOR) {
//				quotattionHdList = quotationSeTrans.getQuotationsByDateRangeForVendor(startDate,
////						Utilities.addDays(endDate, 1), ownerId, moduleId);
			}

		} catch (Exception ex) {

			LOGGER.error("We have received the error " + ex.getMessage());
			baseSerTrans.saveErrorLog(headers, ex, "PMSMasterPriceListService");
			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, quotattionHdList, null);
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}
	
	private PMSContractSupplierPriceHdPojo conertDataFromEntToPojo(PMSContractSupplierPriceHd hd) {
		System.out.println("hd.getContractSupplierPriceDetails().size().."+hd.getContractSupplierPriceDetails().size());
		contractSupplierPriceHdPojo = new PMSContractSupplierPriceHdPojo();
		contractSupplierPriceDtPojo = new PMSContractSupplierPriceDtPojo();

		BeanUtils.copyProperties(hd, contractSupplierPriceHdPojo);
		contractSupplierPriceHdPojo.setSupplierContractId(hd.getPk().getSupplierContractId());
		contractSupplierPriceHdPojo.setSupplierContractNo(hd.getPk().getSupplierContractNo());
		contractSupplierPriceHdPojo.setBargeId(hd.getAdmBarge().getBargeId());
		contractSupplierPriceHdPojo.setVendorId(hd.getAdmVendorId().getVendorId());

		
	
		if (hd.getContractSupplierPriceDetails() != null && hd.getContractSupplierPriceDetails().size() > 0) {

			for (PMSContractSupplierPriceDt details : hd.getContractSupplierPriceDetails()) {
				PMSContractSupplierPriceDtPojo mplDtPojo = new PMSContractSupplierPriceDtPojo();
				BeanUtils.copyProperties(details, mplDtPojo);
				mplDtPojo.setSupplierContractId(details.getPk().getSupplierContractId());
				mplDtPojo.setSupplierContractNo(details.getPk().getSupplierContractNo());
				mplDtPojo.setItemNo(details.getPk().getItemNo());
				mplDtPojo.setMainCode(details.getMainCodeId().getMainCode());
				mplDtPojo.setMainCodeId(details.getMainCodeId().getMainCodeId());
				mplDtPojo.setSubCodeId(details.getAdmSubCode().getSubCodeId());
				
				contractSupplierPriceHdPojo.addContractSupplierPriceDetails(mplDtPojo);
			}
		}
		return contractSupplierPriceHdPojo;
	}
	
	public ResponseEntity<?> saveMpl(Map<String, String> headers, PMSContractSupplierPriceHdPojo req) {
		contractSupplierPriceHdPojo=new PMSContractSupplierPriceHdPojo();
		try {
			contractSupplierPriceHdPojo = mplTransaction.saveMpl(headers, req);
			
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");
			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, contractSupplierPriceHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	public ResponseEntity<?> editMpl(Map<String, String> headers,
			PMSContractSupplierPriceHdPojo req) {
		contractSupplierPriceHdPojo=new PMSContractSupplierPriceHdPojo();
		try {
			contractSupplierPriceHdPojo = mplTransaction.editMpl(headers, req);
			

		} catch (EntityNotFoundException entNotFound) {
			LOGGER.error("We have received the error " + entNotFound.getMessage());
			baseSerTrans.saveErrorLog(headers, entNotFound, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, entNotFound.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);

		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);

		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, contractSupplierPriceHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}

	
	public ResponseEntity<?> getMpl(Map<String, String> headers, BigInteger reqId) {
		contractSupplierPriceHdPojo = null;
		Optional<PMSContractSupplierPriceHd> hd = null;

		try {
			hd = mplTransaction.getMpl(headers, reqId);
			if (hd.isPresent()) {
				contractSupplierPriceHdPojo = conertDataFromEntToPojo(hd.get());
			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, contractSupplierPriceHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	public ResponseEntity<?> getMplByNo(Map<String, String> headers, String supplierNo) {
		contractSupplierPriceHdPojo = null;
		Optional<PMSContractSupplierPriceHd> hd = null;

		try {
			hd = mplTransaction.getMplByNo(headers, supplierNo);
			if (hd.isPresent()) {
				contractSupplierPriceHdPojo = conertDataFromEntToPojo(hd.get());
			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, contractSupplierPriceHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	
	@Transactional
	public ResponseEntity<?> changeRequisitionStatus(Map<String, String> headers, BigInteger supplierContractId
			) {
		boolean transactionSuccess = false;

		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		String trxNo = "";
		String referenceNo = "";
		BigInteger bargeid = BigInteger.ZERO;
		
		try {
			LOGGER.info("request to deleteMpl for Master Price List : " + supplierContractId);
			Optional<PMSContractSupplierPriceHd> hdRec = contractSupplierPriceHdRepository.findMplSupplierContractById(supplierContractId);
			if (hdRec.isPresent()) {
				
					hdRec.get().setIsCancel(1);
					hdRec.get().setCancelBy(userCode);
					hdRec.get().setCancelDate(DateUtilities.getCurrentDate());
				

					bargeid = hdRec.get().getAdmBarge().getBargeId();
					contractSupplierPriceHdRepository.saveAndFlush(hdRec.get());
					transactionSuccess = true;
		
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.MASTER_PRICE_LIST_TABLE_NAME, trxNo, referenceNo, supplierContractId + "", "Cancel",
						bargeid + "");
			}
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK,
				"Successfully Changed The Status for Master Price List  :: " + supplierContractId, null);
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}
	
	public ResponseEntity<?> getContractSupplierVendor(Map<String, String> headers, BigInteger reqId) {
		supplierVendorPojo = new ArrayList<SupplierVendorPojo>();
		
		try {
			supplierVendorPojo = mplTransaction.getContractSupplierVendor(reqId);
			if (supplierVendorPojo.size()==0) {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, supplierVendorPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	
	
	public ResponseEntity<?> getMasterPriceListVendors(Map<String, String> headers,  Map<String, String> map) {
		mplVendorPojoList = new ArrayList<MplVendorPojo>();
		
		try {
			mplVendorPojoList = mplTransaction.getMasterPriceListVendors(headers,map);
			if (mplVendorPojoList.size()==0) {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			baseSerTrans.saveErrorLog(headers, e, "MasterPriceList");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, mplVendorPojoList, null);
		return ResponseEntity.ok().body(resp);
	}

	
}
