package com.pms.mpl.service.transaction;

import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.pms.common.constants.CommonConstants;
import com.pms.common.entity.AdmBarge;
import com.pms.common.entity.AdmMainCode;
import com.pms.common.entity.AdmSubCode;
import com.pms.common.entity.AdmVendor;
import com.pms.common.entity.PMSContractSupplierPriceDt;
import com.pms.common.entity.PMSContractSupplierPriceDtPk;
import com.pms.common.entity.PMSContractSupplierPriceHd;
import com.pms.common.entity.PMSContractSupplierPriceHdPk;
import com.pms.common.pojo.MplVendorPojo;
import com.pms.common.pojo.PMSContractSupplierPriceDtPojo;
import com.pms.common.pojo.PMSContractSupplierPriceHdPojo;
import com.pms.common.pojo.SupplierVendorPojo;
import com.pms.common.service.transaction.BaseServiceTrans;
import com.pms.common.utilities.DateUtilities;
import com.pms.common.utilities.Utilities;
import com.pms.mpl.repository.PMSContractSupplierPriceDtRepository;
import com.pms.mpl.repository.PMSContractSupplierPriceHdRepository;

@Service
public class PMSMasterPriceListServiceTransaction {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSMasterPriceListServiceTransaction.class);

	private final NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	PMSMasterPriceListServiceTransaction(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Autowired
	PMSContractSupplierPriceHdRepository contractSupplierPriceHdRepository;
	@Autowired
	PMSContractSupplierPriceDtRepository contractSupplierPriceDtRepository;

	@Autowired
	BaseServiceTrans baseSerTrans;

	

	
	

	
	
	

	private void convertEntToPojo(PMSContractSupplierPriceHd ent, PMSContractSupplierPriceHdPojo mplHdPojo,
			boolean isList) {

		BeanUtils.copyProperties(ent, mplHdPojo);
		mplHdPojo.setSupplierContractId(ent.getPk().getSupplierContractId());
		mplHdPojo.setSupplierContractNo(ent.getPk().getSupplierContractNo());
		mplHdPojo.setBargeId(ent.getAdmBarge().getBargeId());
		mplHdPojo.setVendorId(ent.getAdmVendorId().getVendorId());
		if (!isList) {
			for (PMSContractSupplierPriceDt mplDt : ent.getContractSupplierPriceDetails()) {
				PMSContractSupplierPriceDtPojo mplDtPojo = new PMSContractSupplierPriceDtPojo();
				BeanUtils.copyProperties(mplDt, mplDtPojo);
				mplDtPojo.setSupplierContractId(mplDt.getPk().getSupplierContractId());
				mplDtPojo.setSupplierContractNo(mplDt.getPk().getSupplierContractNo());
				mplDtPojo.setItemNo(mplDt.getPk().getItemNo());
				mplHdPojo.addContractSupplierPriceDetails(mplDtPojo);
			}
		} else {
			mplHdPojo.setContractSupplierPriceDetails(null);
		}
	}

	@Transactional
	public List<PMSContractSupplierPriceHdPojo> getMasterPriceListByDateRangeForBackOffice(Map<String, String> headers, Map<String, String> map) {
		LOGGER.info("Received getMasterPriceListByDateRangeForBackOffice  ");
		List<PMSContractSupplierPriceHdPojo> mplListPjo = new ArrayList<PMSContractSupplierPriceHdPojo>();
		List<PMSContractSupplierPriceHd> contractSupplierPriceHdList = null;
		mplListPjo = new ArrayList<PMSContractSupplierPriceHdPojo>();
		PMSContractSupplierPriceHdPojo pojo = null;
		
		int transactionId = 0;
		int moduleId = 0;
		int roleId = 0;
		BigInteger ownerId = BigInteger.ZERO;
		Date startDate = null;
		Date endDate = null;
		BigInteger bargeId = BigInteger.ZERO;
		BigInteger vendorId = BigInteger.ZERO;
		BigInteger contractCategoryId = BigInteger.ZERO;
		String processStatusDesc = "";
		
		try {
			
			transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
			moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
			roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
			ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
			
			startDate = Date.valueOf(map.get("startDate"));
			endDate = Date.valueOf(map.get("endDate"));
			bargeId = map.containsKey("bargeId") ? new BigInteger(String.valueOf(map.get("bargeId"))) : BigInteger.ZERO;
			vendorId = map.containsKey("vendorId") ? new BigInteger(String.valueOf(map.get("vendorId"))) : BigInteger.ZERO;
			contractCategoryId = map.containsKey("contractCategoryId") ? new BigInteger(String.valueOf(map.get("contractCategoryId"))) : BigInteger.ZERO;
			processStatusDesc = map.containsKey("processStatusDesc") ? (String)map.get("processStatusDesc") : "";
			processStatusDesc = processStatusDesc == null ? "" : processStatusDesc;
			java.sql.Date futureDate = Utilities.addDays(endDate, 1);
			
			contractSupplierPriceHdList = contractSupplierPriceHdRepository.findAllQuotationsByDateBetween(startDate,
					futureDate, bargeId, vendorId, contractCategoryId);

			if (contractSupplierPriceHdList != null && contractSupplierPriceHdList.size() > 0) {
				for (PMSContractSupplierPriceHd contractSupplierList : contractSupplierPriceHdList) {
					pojo = new PMSContractSupplierPriceHdPojo();
					convertEntToPojo(contractSupplierList, pojo, true);
					mplListPjo.add(pojo);
				}

			}

		} catch (Exception ex) {
			throw ex;
		}
		return mplListPjo;
	}

	@Transactional
	public PMSContractSupplierPriceHdPojo saveMpl(Map<String, String> headers, PMSContractSupplierPriceHdPojo req)
			throws Exception {

		boolean transactionSuccess = false;
		Map<String, Object> map = null;
		BigInteger outNewBtsId = null;
		String outNewDocCode = null;
		PMSContractSupplierPriceHd contractSupplierPriceHd = new PMSContractSupplierPriceHd();
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		PMSContractSupplierPriceHdPk contractSupplierPriceHdPk = new PMSContractSupplierPriceHdPk();
		PMSContractSupplierPriceDt contractSupplierPriceDt = new PMSContractSupplierPriceDt();
		PMSContractSupplierPriceDtPk contractSupplierPriceDtPk = new PMSContractSupplierPriceDtPk();

		try {
			Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());
			map = baseSerTrans.getGeneratedSeqNo(req.getBranchId(), moduleId, transactionId,
					CommonConstants.BANK_ID_ZERO, currentDate, CommonConstants.IN_PREFIX, 0);
			outNewBtsId = new BigInteger(map.get("outNewBtsId").toString());
			outNewDocCode = map.get("outNewDocCode").toString();
			req.setSupplierContractId(outNewBtsId);
			req.setSupplierContractNo(outNewDocCode);
			BeanUtils.copyProperties(req, contractSupplierPriceHd);

			contractSupplierPriceHdPk = new PMSContractSupplierPriceHdPk(req.getSupplierContractId(),
					req.getSupplierContractNo());
			contractSupplierPriceHd.setPk(contractSupplierPriceHdPk);
			contractSupplierPriceHd.setAdmBarge(new AdmBarge(req.getBargeId()));
			contractSupplierPriceHd.setAdmVendorId(new AdmVendor(req.getVendorId()));
			contractSupplierPriceHd.setCreateDate(DateUtilities.getCurrentDate());
			contractSupplierPriceHd.setCreateBy(userCode);
			// contractSupplierPriceHd.setEditDate(currentDateTime);
			contractSupplierPriceHd.setTrxDate(DateUtilities.getCurrentDate());

			LOGGER.info("Created New Supplier Contract Id :- " + outNewBtsId + " & New Supplier Contract No :- "
					+ outNewDocCode);

			for (PMSContractSupplierPriceDtPojo reqDetails : req.getContractSupplierPriceDetails()) {

				contractSupplierPriceDt = new PMSContractSupplierPriceDt();
				contractSupplierPriceDtPk = new PMSContractSupplierPriceDtPk(outNewBtsId, outNewDocCode,
						reqDetails.getItemNo());
				BeanUtils.copyProperties(reqDetails, contractSupplierPriceDt);
				contractSupplierPriceDt.setPk(contractSupplierPriceDtPk);
				
				contractSupplierPriceDt.setMainCodeId(reqDetails.getMainCodeId() == null ? new AdmMainCode(BigInteger.ZERO) : new AdmMainCode(reqDetails.getMainCodeId()));
				contractSupplierPriceDt.setAdmSubCode(reqDetails.getSubCodeId() == null ? new AdmSubCode(BigInteger.ZERO) : new AdmSubCode(reqDetails.getSubCodeId()));
				
				contractSupplierPriceHd.addContractSupplierPriceDetails(contractSupplierPriceDt);
			}
			contractSupplierPriceHdRepository.saveAndFlush(contractSupplierPriceHd);
			transactionSuccess = true;
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.MASTER_PRICE_LIST_TABLE_NAME, outNewDocCode, "" + req.getVendorId(),
						outNewBtsId + "", "Create", req.getBargeId() + "");

			}
		}
		return req;
	}

	@Transactional
	public PMSContractSupplierPriceHdPojo editMpl(Map<String, String> headers, PMSContractSupplierPriceHdPojo req)
			throws Exception {

		boolean transactionSuccess = false;
		Map<String, Object> map = null;
		BigInteger outNewBtsId = null;
		String outNewDocCode = null;
		PMSContractSupplierPriceHd contractSupplierPriceHd = new PMSContractSupplierPriceHd();
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		List<PMSContractSupplierPriceDt> mplDtList = new ArrayList<PMSContractSupplierPriceDt>();
		PMSContractSupplierPriceHdPk contractSupplierPriceHdPk = new PMSContractSupplierPriceHdPk();
		PMSContractSupplierPriceDt contractSupplierPriceDt = new PMSContractSupplierPriceDt();
		PMSContractSupplierPriceDtPk contractSupplierPriceDtPk = new PMSContractSupplierPriceDtPk();
		
		try {
			baseSerTrans.createHistoryRecord(req.getBranchId(), moduleId, transactionId, req.getSupplierContractId());
			Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());

//			outNewBtsId = new BigInteger(map.get("outNewBtsId").toString());
//			outNewDocCode = map.get("outNewDocCode").toString();
//			req.setSupplierContractId(outNewBtsId);
//			req.setSupplierContractNo(outNewDocCode);
			BeanUtils.copyProperties(req, contractSupplierPriceHd);

			contractSupplierPriceHdPk = new PMSContractSupplierPriceHdPk(req.getSupplierContractId(),
					req.getSupplierContractNo());
			contractSupplierPriceHd.setPk(contractSupplierPriceHdPk);
			contractSupplierPriceHd.setAdmBarge(new AdmBarge(req.getBargeId()));
			contractSupplierPriceHd.setAdmVendorId(new AdmVendor(req.getVendorId()));

			contractSupplierPriceHd.setEditDate(DateUtilities.getCurrentDate());
			contractSupplierPriceHd.setEditBy(userCode);

			LOGGER.info("Existing Supplier Contract Id :- " + req.getSupplierContractId()
					+ " & Existing Supplier Contract No :- " + req.getSupplierContractNo());

			for (PMSContractSupplierPriceDtPojo reqDetails : req.getContractSupplierPriceDetails()) {

				contractSupplierPriceDt = new PMSContractSupplierPriceDt();
				contractSupplierPriceDtPk = new PMSContractSupplierPriceDtPk(req.getSupplierContractId(),
						req.getSupplierContractNo(), reqDetails.getItemNo());
				BeanUtils.copyProperties(reqDetails, contractSupplierPriceDt);
				contractSupplierPriceDt.setPk(contractSupplierPriceDtPk);
				contractSupplierPriceDt.setMainCodeId(reqDetails.getMainCodeId() == null ? new AdmMainCode(BigInteger.ZERO) : new AdmMainCode(reqDetails.getMainCodeId()));
				contractSupplierPriceDt.setAdmSubCode(reqDetails.getSubCodeId() == null ? new AdmSubCode(BigInteger.ZERO) : new AdmSubCode(reqDetails.getSubCodeId()));
				contractSupplierPriceHd.addContractSupplierPriceDetails(contractSupplierPriceDt);

				mplDtList.add(contractSupplierPriceDt);
			}

			//contractSupplierPriceDtRepository.deleteAll(mplDtList);
			contractSupplierPriceDtRepository.deleteBySupplierContractId(req.getSupplierContractId());
			contractSupplierPriceDtRepository.flush();
			contractSupplierPriceHdRepository.save(contractSupplierPriceHd);
//			contractSupplierPriceHdRepository.flush();
//			contractSupplierPriceDtRepository.flush();
			LOGGER.info("request to editMpl for Master Price List : " + req.getBargeId());
			transactionSuccess = true;
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.MASTER_PRICE_LIST_TABLE_NAME, outNewDocCode, "" + req.getVendorId(),
						outNewBtsId + "", "Edit", req.getBargeId() + "");

			}
		}
		return req;
	}

	public Optional<PMSContractSupplierPriceHd> getMpl(Map<String, String> headers, BigInteger supplierId) {
		Optional<PMSContractSupplierPriceHd> hd = null;
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		try {
			LOGGER.info("getMpl for Master Price List : " + supplierId);

			if (roleId == 1) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractById(supplierId);
			} else if (roleId == 2) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractIdAndBargeId(supplierId, ownerId);
			} else if (roleId == 3) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractIdAndVendorId(supplierId, ownerId);
			}

		} catch (Exception e) {
			throw e;
		}
		return hd;
	}

	public Optional<PMSContractSupplierPriceHd> getMplByNo(Map<String, String> headers, String supplierNo) {
		Optional<PMSContractSupplierPriceHd> hd = null;
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		try {
			LOGGER.info("getMpl for Master Price List : " + supplierNo);

			if (roleId == 1) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractByNo(supplierNo);
			} else if (roleId == 2) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractNoAndVendorId(supplierNo, ownerId);
			} else if (roleId == 3) {
				hd = contractSupplierPriceHdRepository.findMplSupplierContractNoAndVendorId(supplierNo, ownerId);
			}

		} catch (Exception e) {
			throw e;
		}
		return hd;
	}

	@Transactional
	public List<SupplierVendorPojo> getContractSupplierVendor(BigInteger categoryId) {
		String query = "select PricesHd.VendorId, Vendor.VendorCode, Vendor.VendorName from PMSContractSupplierPriceHd PricesHd, AdmVendor Vendor where PricesHd.VendorId = Vendor.VendorId"
				+ " and PricesHd.ContractCategoryId = :categoryId "
				+ "and PricesHd.ValidFrom <= getdate() and PricesHd.ExpiryDate >= getdate()";

		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("categoryId", "" + categoryId);

		List<SupplierVendorPojo> searchResults = null;
		try {
			searchResults = jdbcTemplate.query(query, queryParams,
					new BeanPropertyRowMapper<>(SupplierVendorPojo.class));

		} catch (Exception e) {
			throw e;
		}

		return searchResults;
	}

	@Transactional
	public List<MplVendorPojo> getMasterPriceListVendors(Map<String, String> headers, Map<String, String> map) {

		Date dateRange = Date.valueOf(map.get("trxDate"));
		String query = "select hd.VendorId, hd.ContactPersonId, hd.SupplierContractId, hd.SupplierContractNo, vdr.vendorName, vdr.vendorCode \r\n"
				+ "from PMSContractSupplierPriceHd hd, admVendor vdr where hd.BranchId = vdr.BranchId \r\n"
				+ "and hd.VendorId = vdr.VendorId\r\n" + "and vdr.BranchId = :branchId\r\n"
				+ "and hd.IsCancel = 0 and  hd.ContractCategoryId = :contractCategoryId and hd.BargeId in (0, :bargeId) \r\n"
				+ "and hd.ValidFrom <= :date and hd.ExpiryDate >= :date order by bargeid desc,hd.CreateDate desc ;";

		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("date", "" + dateRange);
		queryParams.put("contractCategoryId", "" + map.get("contractCategoryId"));
		queryParams.put("bargeId", "" + map.get("bargeId"));
		queryParams.put("branchId", "" + String.valueOf(headers.get(CommonConstants.BRANCH_ID)));

		List<MplVendorPojo> searchResults = null;
		try {
			searchResults = jdbcTemplate.query(query, queryParams, new BeanPropertyRowMapper<>(MplVendorPojo.class));

		} catch (Exception e) {
			throw e;
		}

		return searchResults;
	}

}
