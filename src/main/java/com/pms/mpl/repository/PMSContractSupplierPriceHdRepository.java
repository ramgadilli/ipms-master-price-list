package com.pms.mpl.repository;

import java.math.BigInteger;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pms.common.entity.PMSContractSupplierPriceHd;
import com.pms.common.entity.PMSContractSupplierPriceHdPk;

public interface PMSContractSupplierPriceHdRepository
		extends JpaRepository<PMSContractSupplierPriceHd, PMSContractSupplierPriceHdPk> {
	
	@Query("select q from PMSContractSupplierPriceHd q  where q.trxDate between :startDate and :endDate  "
			+ " and q.contractCategoryId = Case when :contractCategoryId > java.math.BigInteger.ZERO then :contractCategoryId else q.contractCategoryId end  " +
			" and q.admBarge.bargeId = Case when :bargeId > java.math.BigInteger.ZERO then :bargeId else q.admBarge.bargeId end  " +
			" and q.admVendorId.vendorId = Case When :vendorId > java.math.BigInteger.ZERO then :vendorId Else q.admVendorId.vendorId end  order by q.pk.supplierContractId asc")
	List<PMSContractSupplierPriceHd> findAllQuotationsByDateBetween(Date startDate, Date endDate, BigInteger bargeId, BigInteger vendorId, BigInteger contractCategoryId );
	
	@Query("SELECT e from PMSContractSupplierPriceHd e where e.pk.supplierContractId=:supplierContractId and e.admBarge.bargeId in (0,:bargeId)   order by e.pk.supplierContractId asc")
	Optional<PMSContractSupplierPriceHd> findMplSupplierContractIdAndBargeId(BigInteger supplierContractId, BigInteger bargeId);
	
	@Query("SELECT e from PMSContractSupplierPriceHd e where e.pk.supplierContractId=:supplierContractId and e.admVendorId.vendorId=:vendorId   order by e.pk.supplierContractId asc")
	Optional<PMSContractSupplierPriceHd> findMplSupplierContractIdAndVendorId(BigInteger supplierContractId, BigInteger vendorId);
	
	@Query("SELECT e from PMSContractSupplierPriceHd e where e.pk.supplierContractId=:supplierContractId   order by e.pk.supplierContractId asc")
	Optional<PMSContractSupplierPriceHd> findMplSupplierContractById(BigInteger supplierContractId);
	
	
	
	@Query("SELECT e from PMSContractSupplierPriceHd e where e.pk.supplierContractNo=:supplierContractNo and e.admBarge.bargeId in (0,:bargeId)   order by e.pk.supplierContractId asc")
	Optional<PMSContractSupplierPriceHd> findMplSupplierContractNoAndBargeId(String supplierContractNo, BigInteger bargeId);
	
	@Query("SELECT e from PMSContractSupplierPriceHd e where e.pk.supplierContractNo=:supplierContractNo and e.admVendorId.vendorId=:vendorId   order by e.pk.supplierContractId asc")
	Optional<PMSContractSupplierPriceHd> findMplSupplierContractNoAndVendorId(String supplierContractNo, BigInteger vendorId);
	
	@Query("SELECT e from PMSContractSupplierPriceHd e where e.pk.supplierContractNo=:supplierContractNo   order by e.pk.supplierContractId asc")
	Optional<PMSContractSupplierPriceHd> findMplSupplierContractByNo(String supplierContractNo);



}
