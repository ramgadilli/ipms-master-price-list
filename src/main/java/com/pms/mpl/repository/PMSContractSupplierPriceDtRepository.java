package com.pms.mpl.repository;

import java.math.BigInteger;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pms.common.entity.PMSContractSupplierPriceDt;
import com.pms.common.entity.PMSContractSupplierPriceDtPk;

public interface PMSContractSupplierPriceDtRepository
		extends JpaRepository<PMSContractSupplierPriceDt, PMSContractSupplierPriceDtPk> {
	
	
	@Transactional
	@Modifying
	@Query("delete from PMSContractSupplierPriceDt q where q.pk.supplierContractId=:supplierContractId")
	void deleteBySupplierContractId(@Param("supplierContractId") BigInteger supplierContractId);


}
